<?php

function random_str($length = 15)
{
    return substr(str_shuffle(str_repeat($x='01234567899abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))),1,$length);
}

function getMenu()
{
    return [
        'home' => [
            'key' => 'home',
            'name' => 'Beranda',
            'link' => 'home',
            'icon' => 'home'
        ],
        'master' => [
            'key' => 'master',
            'name' => 'Master',
            'link' => '',
            'icon' => 'tasks',
            'child' => [
                'role' => [
                    'key' => 'role',
                    'name' => 'Role',
                    'link' => 'masters/role'
                ],
                'produk' => [
                    'key' => 'produk',
                    'name' => 'Data Produk',
                    'link' => 'masters/produk'
                ],
                'kategori' => [
                    'key' => 'kategori',
                    'name' => 'Data Kategori Produk',
                    'link' => 'masters/kategori'
                ],
                'jenis' => [
                    'key' => 'jenis',
                    'name' => 'Data Jenis Produk',
                    'link' => 'masters/jenis'
                ],
                'desa' => [
                    'key' => 'desa',
                    'name' => 'Data Desa',
                    'link' => 'masters/desa'
                ],
                'users' => [
                    'key' => 'users',
                    'name' => 'Data User',
                    'link' => 'masters/users'
                ]
            ]
        ],
        'transaksi' => [
            'key' => 'transaksi',
            'name' => 'Transaksi',
            'link' => '',
            'icon' => 'coins',
            'child' => [
                'pembayaran' => [
                    'key' => 'pembayaran',
                    'name' => 'Pembayaran',
                    'link' => 'pembayaran'
                ]
            ]
        ],
        'statistik' => [
            'key' => 'statistik',
            'name' => 'Statistik',
            'icon' => 'chart-line',
            'link' => 'statistik'
        ],
        'laporan' => [
            'key' => 'laporan',
            'name' => 'Laporan',
            'link' => '',
            'icon' => 'book',
            'child' => [
                'laporan' => [
                    'key' => 'laporan',
                    'name' => 'Laporan',
                    'link' => 'laporan'
                ],
                'kode_laporan' => [
                    'key' => 'kode_laporan',
                    'name' => 'Kode Laporan',
                    'link' => 'laporan/setCode'
                ]
            ]
        ],
        'pengaturan' => [
            'key' => 'pengaturan',
            'name' => 'Pengaturan Aplikasi',
            'icon' => 'cogs',
            'link' => 'pengaturan'
        ],
        'logout' => [
            'key' => 'logout',
            'name' => 'Logout',
            'icon' => 'sign-out-alt',
            'link' => 'auth/logout'
        ]
    ];
}

function setMessage($message, $type)
{
    //type adalah tipe dari alert
    //message pesan nya
    $ci = get_instance();
    $message = $ci->session->set_flashdata('message', '<div class="alert alert-' . $type . ' alert-dismissible fade show" role="alert">' . $message . '               
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>');
}

function setPagination($limit = 10, $rows = null, $url, $model = null, $method = null)
{
    //instance ci
    $ci = get_instance();
    //load library
    $ci->load->library('pagination');

    $config['base_url'] = site_url($url);
    $config['total_rows'] = $rows;
    $config['per_page'] = $limit;
    $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;

    //styling
    $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
    $config['full_tag_close'] = '</ul></nav>';

    $config['first_link'] = 'First';
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';

    $config['last_link'] = 'Last';
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';

    $config['attributes'] = array('class' => 'page-link');


    //init
    $ci->pagination->initialize($config);
}

function toRupiah($val)
{
    $x = "Rp " . number_format($val, 0, ',', '.');
    return $x;
}

function settingSIM()
{
    $ci = get_instance();
    $ci->load->model('M_setting', 'setting');

    $setting = $ci->setting->get()->result_array();

    $a_data = array();
    foreach ($setting as $val) {
        $a_data[$val['kode']] = $val['isi'];
    }

    return $a_data;
}

function getPembeli()
{
    $ci = get_instance();
    $email = $ci->session->userdata('email');
    $query = "SELECT p.idpembeli,p.namapembeli,p.tanggal_lahir,p.telepon,p.jk FROM users u JOIN pembeli p USING(idpembeli) WHERE u.email='$email'";

    return $ci->db->query($query);
}

function getCart()
{
    $ci = get_instance();

    if (getPembeli()) {
        $q = "SELECT * FROM keranjang JOIN produk USING(idproduk) WHERE idpembeli=" . "'" .  getPembeli()->row_array()['idpembeli'] . "'";
        $cart = $ci->db->query($q);
    }

    return $cart;
}

function getTotalItemCart()
{
    $cart = getCart()->result_array();
    $total = 0;

    foreach ($cart as $val) {
        $total += $val['jumlah'];
    }

    return $total;
}

function getWishList()
{
    $ci = get_instance();

    if (getPembeli()) {
        $q = "SELECT * FROM favorit JOIN produk USING(idproduk) WHERE idpembeli=" . "'" .  getPembeli()->row_array()['idpembeli'] . "'";
        $fav = $ci->db->query($q);
    }

    return $fav;
}

function dd($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die;
}

function isLoggedIn($token, $callback, $isAuthorizedCheck = false)
{
    $ci = get_instance();
    $query = "SELECT token, valid_until FROM login_attempts WHERE token='$token'";


    $login_data = $ci->db->query($query)->row_array(); 
    
    if($login_data){
        if(strtotime($login_data['valid_until']) < time()){
            return $callback(false);
        }else{
            if($isAuthorizedCheck){
                if(getPembeli()->row_array()['idpembeli']){
                    return redirect('errors/unauthorized');
                }else{
                    return $callback(true);    
                }
            }else{
                return $callback(true);
            }
        }
    }else{
        return $callback(false);
    }
}

function getUserDetails($email = null)
{
    $ci = get_instance();
    
    $email = $email? $email : $ci->session->userdata('email');

    $user = $ci->db->query("SELECT * FROM users u LEFT JOIN userrole us ON u.iduser=us.iduser WHERE u.email='$email'")->row_array();

    return $user;
}

function unset_list($session = array()) : void
{
    $ci = get_instance();

    foreach ($session as $val) {
        $ci->session->unset_userdata($val);
    }
}
