<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kategori extends MY_Model
{
    protected $table = 'kategori';
    protected $schema = '';
    public $key = 'idkategori';
    public $value = 'namakategori';

    function __construct()
    {
        parent::__construct();
    }
}
