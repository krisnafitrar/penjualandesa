<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_keranjang extends MY_Model
{
    protected $table = 'keranjang';
    protected $schema = '';
    public $key = 'idkeranjang';
    public $value = '';

    function __construct()
    {
        parent::__construct();
    }

    public function getKeranjangDetail($idpembeli)
    {
        return $this->db->select('*')
                        ->from($this->getTable() . ' k')
                        ->join('produk p', 'p.idproduk=k.idproduk')
                        ->where('idpembeli', $idpembeli)
                        ->get();
    }
}
