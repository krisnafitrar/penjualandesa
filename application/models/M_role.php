<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_role extends MY_Model
{
    protected $table = 'role';
    protected $schema = '';
    public $key = 'idrole';
    public $value = 'namarole';

    function __construct()
    {
        parent::__construct();
    }
}
