<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_alamat extends MY_Model
{
    protected $table = 'alamatpembeli';
    protected $schema = '';
    public $key = 'idalamat';
    public $value = "alamat";

    function __construct()
    {
        parent::__construct();
    }

    public function joinAll($id){
        $query = "SELECT * FROM alamatpembeli ap JOIN provinces pv ON ap.idprovinsi=pv.province_id JOIN cities ct ON ap.idkota=ct.city_id JOIN subdistricts sb ON ap.idkecamatan=sb.subdistrict_id WHERE ap.idalamat='$id'";
        return $this->db->query($query);
    }
    
    public function joinById($id){
        $query = "SELECT * FROM alamatpembeli ap JOIN provinces pv ON ap.idprovinsi=pv.province_id JOIN cities ct ON ap.idkota=ct.city_id JOIN subdistricts sb ON ap.idkecamatan=sb.subdistrict_id WHERE ap.idpembeli='$id'";
        return $this->db->query($query);
    }

}
