<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kota extends MY_Model
{
    protected $table = 'cities';
    protected $schema = '';
    public $key = 'city_id';
    public $value = "city_name";

    function __construct()
    {
        parent::__construct();
    }

    public function getBy($where)
    {
        $this->db->where($where);
        return $this->db->get($this->getTable());
    }

}
