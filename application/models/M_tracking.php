<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_tracking extends MY_Model
{
    protected $table = 'tracking';
    protected $schema = '';
    public $key = 'idtracking';
    public $value = 'status';
}
