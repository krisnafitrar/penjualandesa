<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_jenis extends MY_Model
{
    protected $table = 'jenisproduk';
    protected $schema = '';
    public $key = 'idjenisproduk';
    public $value = 'namajenisproduk';

    function __construct()
    {
        parent::__construct();
    }
}
