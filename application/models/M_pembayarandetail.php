<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pembayarandetail extends MY_Model
{
    protected $table = 'pembayarandetail';
    protected $schema = '';
    public $key = 'idpembayaran,idproduk';
    public $value = 'idpembayaran,idproduk';

    public function getDetailPembayaran($id)
    {
        $query = "SELECT pb.idpembayaran,pb.idpembeli,pb.delivery_cost,pb.tgl,pb.time,pb.status_pembayaran,pb.invoice,p.namaproduk,p.idproduk,p.harga,p.foto,d.namadesa,jp.namajenisproduk,ap.penerima,ap.jenisalamat,ap.alamat,ap.idkecamatan,ap.kodepos,pd.jumlah,sp.statuspesanan FROM pembayaran pb JOIN pembayarandetail pd USING(idpembayaran) JOIN produk p ON pd.idproduk=p.idproduk JOIN desa d ON p.iddesa=d.iddesa JOIN jenisproduk jp ON p.idjenisproduk=jp.idjenisproduk JOIN alamatpembeli ap ON pb.idalamat=ap.idalamat JOIN statuspesanan sp ON pb.statuspesanan=sp.idstatus WHERE idpembayaran=" . $id;
        
        return $this->db->query($query);
    }
}
