<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_produk extends MY_Model
{
    protected $table = 'produk';
    protected $schema = '';
    public $key = 'idproduk';
    public $value = 'namaproduk';

    function __construct()
    {
        parent::__construct();
    }

    public function getProduk()
    {
        $query = "SELECT * FROM produk JOIN desa USING(iddesa) JOIN kategori USING(idkategori) JOIN jenisproduk USING(idjenisproduk) LEFT JOIN favorit USING(idproduk) LIMIT 8";
        return $this->db->query($query);
    }

    public function getDetailProduk($slug)
    {
        $query = "SELECT * FROM produk JOIN desa USING(iddesa) JOIN kategori USING(idkategori) JOIN jenisproduk USING(idjenisproduk) WHERE slug='$slug'";
        return $this->db->query($query);
    }

    public function getProdukLimit($limit, $start, $keyword = null, $kategori = null, $sort = null)
    {
        $query = "SELECT * FROM produk JOIN desa USING(iddesa) JOIN kategori USING(idkategori) JOIN jenisproduk USING(idjenisproduk) LEFT JOIN favorit USING(idproduk)";

        $where = null;

        if ($keyword) {
            $where= " WHERE namaproduk LIKE '%$keyword%'";
        }

        if($kategori){
            if($where){
                $where .= " AND slug_kategori='$kategori'";
            }else{
                $where = " WHERE slug_kategori='$kategori'";
            }
        }

        $orderBy = null;

        if($sort){
            switch ($sort) {
                case 0:
                    $orderBy = "";    
                    break;
                case 1:
                    $orderBy = " ORDER BY harga ASC";
                    break;
                case 2:
                    $orderBy = " ORDER BY harga DESC";
                    break;
            }
        }

        $query .= $where . ($sort != null ? $orderBy : "");

        $this->db->limit($limit, $start);
        return $this->db->query($query)->result_array();
    }
}
