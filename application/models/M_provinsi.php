<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_provinsi extends MY_Model
{
    protected $table = 'provinces';
    protected $schema = '';
    public $key = 'province_id';
    public $value = "province_name";

    function __construct()
    {
        parent::__construct();
    }

    public function getBy($where)
    {
        $this->db->where($where);
        return $this->db->get($this->getTable());
    }

}
