<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kecamatan extends MY_Model
{
    protected $table = 'subdistricts';
    protected $schema = '';
    public $key = 'subdistrict_id';
    public $value = "subdistrict_name";

    function __construct()
    {
        parent::__construct();
    }

    public function getBy($where)
    {
        $this->db->where($where);
        return $this->db->get($this->getTable());
    }

}
