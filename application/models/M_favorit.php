<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_favorit extends MY_Model
{
    protected $table = 'favorit';
    protected $schema = '';
    public $key = 'idfavorit';
    public $value = "idpembeli||' - '||idproduk";

    function __construct()
    {
        parent::__construct();
    }

    public function favoritPembeli($idpembeli)
    {
        return $this->db->select('pr.*,ds.*')
                        ->from($this->getTable() . ' f')
                        ->join('produk pr', 'pr.idproduk=f.idproduk')
                        ->join('pembeli p','p.idpembeli=f.idpembeli')
                        ->join('desa ds', 'ds.iddesa=pr.iddesa')
                        ->where('f.idpembeli',$idpembeli)
                        ->get();
    }
}
