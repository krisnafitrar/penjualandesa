<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pembayaran extends MY_Model
{
    protected $table = 'pembayaran';
    protected $schema = '';
    public $key = 'idpembayaran';
    public $value = 'jumlah';

    public function getPembayaran()
    {
        $q = "SELECT p.idpembayaran,p.jumlah,p.ongkir,p.status,p.buktibayar,p.invoice,p.tgl,p.time,pm.namapembeli FROM pembayaran p JOIN pembayarandetail pd ON p.idpembayaran=pd.idpembayaran JOIN pembeli pm ON p.idpembeli=pm.idpembeli GROUP BY p.idpembayaran";
        return $this->db->query($q);
    }

    public function getStatistik($from, $to)
    {
        return $this->db->query($this->queryStatistik($from, $to));
    }

    public function queryStatistik($from, $to)
    {
        return "SELECT SUM(pd.jumlah) AS terjual,p.tgl,pr.namaproduk,pr.idproduk FROM pembayaran p JOIN pembayarandetail pd ON p.idpembayaran=pd.idpembayaran JOIN produk pr ON pd.idproduk=pr.idproduk WHERE (p.tgl BETWEEN '$from' AND '$to') GROUP BY pd.idproduk";
    }

    public function getLaporan($from, $to)
    {
        $q = "SELECT SUM(p.jumlah) AS jmlpayment, SUM(p.ongkir) AS jmlongkir,p.tgl FROM pembayaran p WHERE (p.tgl BETWEEN '$from' AND '$to') AND p.statuspesanan=3 GROUP BY p.tgl";
        return $this->db->query($q);
    }
}
