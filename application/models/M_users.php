<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_users extends MY_Model
{
    protected $table = 'users';
    protected $schema = '';
    public $key = 'iduser';
    public $value = 'namauser';

    function __construct()
    {
        parent::__construct();
    }

    public function getUsers($limit, $start, $keyword = null)
    {
        if ($keyword) {
            $this->db->like('email', $keyword);
        }
        return $this->db->get('users', $limit, $start)->result_array();
    }
}
