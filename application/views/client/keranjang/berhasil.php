<section class="ftco-section testimony-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate text-center">
                <span class="subheading">Chekout Berhasil</span>
                <h2 class="mb-4">Silakan melakukan pembayaran</h2>
            </div>
            <div class="col-md-7 heading-section ftco-animate">
                <p align="center">Jumlah yang harus di bayar adalah <strong><?= toRupiah($pembayaran['jumlah'] + $pembayaran['ongkir']) ?></strong></p>
                <p>Pembayaran harap di transfer melelui rekening di bawah ini</p>
                <?= $settingSIM['pembayaran'] ?>
                <p>Setelah melakukan pembayaran, harap melakukan konfirmasi dan upload bukti pembayaran melalui menu pesanan saya</p>
            </div>
        </div>
    </div>
</section>