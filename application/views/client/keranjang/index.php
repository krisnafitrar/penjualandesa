<section class="ftco-section ftco-cart">
	<form action="<?= site_url('keranjang/checkoutberhasil') ?>" method="POST" id="form_checkout">
		<div class="container">
			<?php if ($keranjang) : ?>
			<div class="row">
				<div class="col-md-12 ftco-animate">
					<?= $this->session->flashdata('message'); ?>
					<div class="cart-list">
						<table class="table">
							<thead class="thead-primary">
								<tr class="text-center">
									<th>&nbsp;</th>
									<th>&nbsp;</th>
									<th>Produk</th>
									<th>Harga</th>
									<th>Jumlah</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$total = 0;
									$grand_total = 0;
									?>
								<?php foreach ($keranjang as $kr) : ?>
								<tr class="text-center">
									<td class="product-remove"><a href="" data-type="btn-remove"
											data-produk="<?= $kr['idproduk'] ?>"><span class="ion-ios-close"></span></a>
									</td>

									<td class="image-prod">
										<div class="img"
											style="background-image:url(<?= base_url('assets/img/produk/') . $kr['foto'] ?>);">
										</div>
									</td>

									<td class="product-name">
										<h3><?= $kr['namaproduk'] ?></h3>
									</td>

									<td class="price"><?= toRupiah($kr['harga']) ?></td>

									<td class="quantity">
										<div class="input-group mb-3">
											<input type="number" id="inputjumlah" data-id="<?= $kr['idkeranjang'] ?>"
												name="quantity_<?= $kr['idkeranjang'] ?>"
												class="quantity form-control input-number" value="<?= $kr['jumlah'] ?>"
												min="1" max="10">
										</div>
									</td>

									<td class="total"><?= toRupiah($kr['harga'] * $kr['jumlah']) ?></td>
									<?php
											$grand_total += $kr['harga'] * $kr['jumlah'];
											?>
								</tr><!-- END TR-->
								<input type="hidden" name="keranjang[]" id="keranjang"
									value="<?= $kr['idkeranjang'] ?>">
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row justify-content-end">
				<div class="col-lg-8 mt-5 cart-wrap ftco-animate">
					<div class="cart-total">
						<h3>Alamat Pengiriman</h3>
						<p>Pilih alamat pengiriman</p>
						<div class="row">
							<?php
								if (count($alamat) > 0) {
									foreach ($alamat as $val) { ?>
							<div class="col-md-12">
								<div class="radio">
									<label class="col-md-12">
										<input type="radio" class="col-md-1" name="alamat"
											data-provinsi="<?= $val['idprovinsi'] ?>" data-kota="<?= $val['idkota'] ?>"
											data-kecamatan="<?= $val['idkecamatan'] ?>"
											id="options<?= $val['idalamat'] ?>" value="<?= $val['idalamat'] ?>">
										<label for="options<?= $val['idalamat'] ?>" class="col-md-10">
											<div class="card col-md-12">
												<div class="card-body">
													<h5 class="card-title"><?= $val['jenisalamat'] ?></h5>
													<p class="card-text"><?= $val['alamat'] ?>, <?= $val['kodepos'] ?>,
														<?= $val['subdistrict_name'] ?>, <?= $val['city_name'] ?>,
														<?= $val['province_name'] ?></p>
												</div>
											</div>
										</label>
									</label>
								</div>
								<hr>
							</div>
							<?php } ?>
							<?php } else { ?>
							<p class="ml-3 text-danger">alamat belum ada, silakan tambah alamat terlebih dahulu!</p>
							<a href="<?= site_url('profil/alamat') ?>" class="btn btn-info btn-sm ml-3">Tambah
								alamat</a>
							<?php } ?>
						</div>
					</div>
					<div class="cart-total mt-3">
						<h3>Metode Pengiriman</h3>
						<p>Pilih metode pengiriman</p>
						<div class="row">
							<div class="col">
								<select name="kurir" id="kurir" class="form-control">
									<option value="">-- Pilih Kurir --</option>
									<option value="jne">Jalur Nugraha Ekakurir - JNE</option>
									<option value="pos">POS Indonesia</option>
									<option value="tiki">Titipan Kilat - TIKI</option>
								</select>
								<div class="d-flex justify-content-center" id="loading-space">
								</div>
							</div>
						</div>
						<div class="row mt-3">
							<div class="col">
								<select name="paketpengiriman" id="paketpengiriman" class="form-control" disabled>
									<option value="">-- Pilih Paket Pengiriman --</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5 cart-wrap ftco-animate">
					<div class="cart-total mb-3">
						<h3>Total Belanja</h3>
						<p class="d-flex">
							<span>Subtotal</span>
							<span><?= toRupiah($grand_total) ?></span>
						</p>
						<hr>
						<p class="d-flex">
							<span>Ongkir</span>
							<span id="totalongkir"><?= toRupiah(0) ?></span>
						</p>
						<hr>
						<p class="d-flex total-price">
							<span>Grand Total</span>
							<span id="labelgrandtotal"><?= toRupiah($grand_total) ?></span>
						</p>
					</div>
					<p><button type="button" data-type="btn-checkout" class="btn btn-primary py-3 px-4">Checkout
							Sekarang</button></p>
				</div>
			</div>
			<?php else : ?>
			<div class="col-lg-12 mt-3">
				<div class="text-center">
					<img src="<?= base_url('assets/img/illustration/cart.png') ?>" class="img-fluid img-produk"
						width="500px" height="300px" alt="">
					<h3 class="text-secondary mt-3"><b>Keranjang Kamu Kosong</b></h3>
					<p>Hmm kamu belum menambahkan produk ke keranjang, ayo segera tambahkan.</p>
					<a href="<?= base_url('produk') ?>" class="btn btn-primary">Cari Produk Sekarang</a>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<input type="hidden" name="ongkir" id="inputongkir" value="">
		<input type="hidden" name="courier_id" id="courier_id" value="">
		<input type="hidden" name="courier_service" id="courier_service" value="">
		<input type="hidden" name="destination_city" id="destination_city" value="">
		<input type="hidden" name="courier_payloads" id="courier_payloads" value="">
		<input type="hidden" name="act" id="act">
	</form>
</section>

<script>
	$(function () {
		$('[data-type="btn-checkout"]').click(function () {
			var id = $(this).attr('data-id');
			var check = $('[name="alamat"]:checked').length;

			if (check > 0) {
				let kurir = $('#kurir').val();
				if (kurir != "") {
					bootbox.confirm("Apakah anda ingin melakukan checkout?", function (result) {
						if (result) {
							$('#form_checkout #act').val('checkout');
							$('#form_checkout').submit();
						}
					});
				} else {
					bootbox.alert("Harap memilih kurir terlebih dahulu!");
				}
			} else {
				bootbox.alert("Harap memilih alamat terlebih dahulu!");
			}
		})
	});

	$('[data-type=btn-remove]').click(function () {
		var idproduk = $(this).attr('data-produk');
		var idpembeli = "<?= getPembeli()->row_array()['idpembeli'] ?>";
		var act = 'del';

		$.ajax({
			url: "<?= base_url('keranjang/cart') ?>",
			type: 'post',
			data: {
				idproduk: idproduk,
				idpembeli: idpembeli,
				act: act
			},
			success: function () {
				document.location.href = "<?= base_url('keranjang') ?>";
			}
		});

	});

	$(document).on('input', '#inputjumlah', function () {
		var qty = $(this).val();
		var id = $(this).attr('data-id');
		var act = 'update';

		$.ajax({
			url: "<?= base_url('keranjang/cart') ?>",
			type: 'post',
			data: {
				idkeranjang: id,
				jumlah: qty,
				act: act
			},
			success: function () {
				document.location.href = "<?= base_url('keranjang') ?>";
			}
		});

	});

	$('#kurir').on('change', function () {
		var kurir = $(this).val();
		var check = $('[name="alamat"]:checked').length;
		var provinsi = $('[name="alamat"]:checked').attr('data-provinsi');
		var kotaTujuan = $('[name="alamat"]:checked').attr('data-kota');
		var kecamatan = $('[name="alamat"]:checked').attr('data-kecamatan');
		var weight = 1000;
		var kotaAsal = parseInt("<?= $origin_city ?>"); //contoh code kota jogja 

		if (check > 0) {
			if (kurir != "") {
				$.ajax({
					url: "<?= base_url('keranjang/cekOngkir') ?>",
					type: 'post',
					data: {
						origin: kotaAsal,
						destination: kotaTujuan,
						weight: weight,
						courier: kurir
					},
					beforeSend: function () {
						let loading = $('#loading-space');
						let loadSpinner = `<div class="spinner-grow text-primary" role="status">
											<span class="sr-only">Loading...</span>
										</div>
										<div class="spinner-grow text-secondary" role="status">
											<span class="sr-only">Loading...</span>
										</div>
										<div class="spinner-grow text-success" role="status">
											<span class="sr-only">Loading...</span>
										</div>
										<div class="spinner-grow text-danger" role="status">
											<span class="sr-only">Loading...</span>
										</div>`;
						loading.html(loadSpinner);
					},
					success: function (result) {
						$('#loading-space').html('');
						let paketPengiriman = $('#paketpengiriman');
						paketPengiriman.prop('disabled', false);

						let data = JSON.parse(result);
						$('#destination_city').val(kotaTujuan);
						$('#courier_id').val(kurir);
						$('#courier_payloads').val(JSON.stringify(data));

						let option = '<option value="">-- Pilih Paket Pengiriman --</option>';

						data[0].costs.forEach(element => {
							option +=
								`<optgroup label="` +
								`${element.service} - ${element.description}` +
								`"><option value="` + element.service + "-" + element.cost[0]
								.value +
								`">${element.cost[0].etd} ${element.cost[0].etd.includes("HARI") ? '' : 'hari'} - Rp ${ toRupiah(element.cost[0].value) } </option></optgroup>`;
						});

						paketPengiriman.html(option);

					}
				});
			} else {
				bootbox.alert("Harap memilih kurir yang valid!");
			}
		} else {
			bootbox.alert("Harap memilih alamat terlebih dahulu!");
		}
	});

	$('#paketpengiriman').on('change', function () {
		let paketPengirimanInfo = $(this).val().split("-");
		let ongkir = $('#totalongkir');
		let labelGrandtotal = $('#labelgrandtotal');
		let totalOngkir = parseInt(paketPengirimanInfo[1]);
		let inputOngkir = $('#inputongkir').val(totalOngkir);
		let grandTotal = parseInt("<?= $grand_total ?>");

		$('#courier_service').val(paketPengirimanInfo[0]);

		grandTotal += totalOngkir;

		ongkir.html(`Rp ${ toRupiah(totalOngkir) }`);
		labelGrandtotal.html(`Rp ${ toRupiah(grandTotal) }`);

	});

	function toRupiah(angka) {
		var reverse = angka.toString().split('').reverse().join(''),
			ribuan = reverse.match(/\d{1,3}/g);
		ribuan = ribuan.join('.').split('').reverse().join('');
		return ribuan;
	}

</script>
