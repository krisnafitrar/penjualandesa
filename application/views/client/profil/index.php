<div class="container">
	<div class="card mb-3 mx-auto" style="max-width: 540px;">
		<div class="row no-gutters">
			<div class="col-md-4">
				<img src="<?= base_url('assets/img/default.png') ?>" class="card-img" alt="">
			</div>
			<div class="col-md-8">
				<div class="card-body">
					<h5 class="card-title"><?= $users['nama'] ?></h5>
					<p class="card-text"><?= $users['email'] ?></p>
					<small>Kelengkapan profil</small>
					<div class="progress">
						<div class="progress-bar" role="progressbar" style="width: <?= $persentase_profil ?>%;"
							aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= $persentase_profil ?>%</div>
					</div>
					<p class="card-text"><small class="text-muted">Bergabung pada
							<?= $users['created_at'] ?></small></p>
				</div>
			</div>
		</div>
	</div>
</div>
