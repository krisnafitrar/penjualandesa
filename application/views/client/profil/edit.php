<div class="row">
	<div class="col-md-2">
	</div>
	<div class="col-md-8">
		<?= $this->session->flashdata('message') ?>
		<form action="<?= base_url('profil/edit') ?>" method="POST">
			<div class="form-group">
				<label for="nama">Nama</label>
				<input type="text" class="form-control" name="nama" id="nama" value="<?= $pembeli['namapembeli'] ?>">
			</div>
			<div class="form-group">
				<label for="nama">Tanggal Lahir</label>
				<input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir"
					value="<?= $pembeli['tanggal_lahir'] ?>">
			</div>
			<div class="form-group">
				<label for="telepon">Telepon</label>
				<input type="text" class="form-control" name="telepon" id="telepon" value="<?= $pembeli['telepon'] ?>">
			</div>
			<div class="form-group">
				<label for="jk">Jenis Kelamin</label>
				<select class="form-control" name="jk" id="jk">
					<?php foreach ($jk as $key => $val) : ?>
					<option value="<?= $key ?>" <?= $key == $pembeli['jk'] ? 'selected':'' ?>><?= $val ?></option>
					<?php endforeach ?>
				</select>
			</div>
			<input type="hidden" value="<?= $pembeli['idpembeli'] ?>" name="idpembeli">
			<button type="submit" data-type="save" class="btn btn-primary py-3 px-4">Simpan</button>
		</form>
	</div>
	<div class="col-md-2">
	</div>
</div>
