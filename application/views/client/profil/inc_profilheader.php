<div class="row justify-content-center">
	<div class="col-md-10 mb-5 text-center">
		<ul class="product-category">
			<li><a href="<?= site_url('profil') ?>" class="<?= $active == "profil" ? 'active' : ''  ?> mb-2">Profil</a>
			</li>
			<li><a href="<?= site_url('profil/edit') ?>" class="<?= $active == "edit" ? 'active' : ''  ?> mb-2">Edit
					Profil</a></li>
			<li><a href="<?= site_url('profil/password') ?>"
					class="<?= $active == "password" ? 'active' : ''  ?> mb-2">Ganti Password</a></li>
			<li><a href="<?= site_url('profil/alamat') ?>"
					class="<?= $active == "alamat" ? 'active' : ''  ?> mb-2">Alamat</a></li>
		</ul>
	</div>
</div>
