<div class="container">
	<div class="row">
		<div class="col-md-2">
		</div>
		<div class="col-md-8">
			<form method="POST" id="form_data">
				<?php foreach ($a_kolom as $col) { ?>
				<?php if (isset($col['type']) && $col['type'] == 'S2') : ?>
				<div class="form-group <?= $col['kolom'] ?>">
					<label for="<?= $col['kolom'] ?>"><?= $col['label'] ?></label>
					<?php if(!empty($row)):?>
					<input type="text" class="form-control" name="" id=""
						value="<?= !empty($row) ? $row[$col['real_col']] : '' ?>"
						placeholder="Masukkan <?= $col['label'] ?>" <?= $col['required'] == true ? 'required' : '' ?>>
					<?php endif;?>
					<select name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" class="form-control"
						<?= $col['default'] == 'enabled' ? '' : 'disabled' ?>></select>
				</div>
				<?php else : ?>
				<div class="form-group">
					<label for="<?= $col['kolom'] ?>"><?= $col['label'] ?></label>
					<input type="text" class="form-control" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>"
						value="<?= !empty($row) ? $row[$col['kolom']] : '' ?>"
						placeholder="Masukkan <?= $col['label'] ?>" <?= $col['required'] == true ? 'required' : '' ?>>
				</div>
				<?php endif; ?>
				<?php } ?>
				<button type="button" data-type="save" class="btn btn-primary py-3 px-4">Simpan</button>
				<input type="hidden" name="act" id="act">
				<input type="hidden" name="key" id="key">
			</form>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>
<script>
	$(function () {
		$('[data-type="save"]').click(function () {
			bootbox.confirm("Apakah anda yakin akan menyimpan data ini?", function (result) {
				if (result) {
					let key = "<?= !empty($row) ? $row['idalamat'] : null ?>";
					$('#form_data #act').val('save');
					$('#form_data #key').val(key);
					$('#form_data').submit();
				}
			});
		})
	});


	$('#idprovinsi').on('change', function () {
		let provinsi = $('#idprovinsi').val();

		if (provinsi != null) {
			$('#idkota').removeAttr('disabled');
		} else {
			let kotaVal = $('#idkota').val();
			let kecamatanVal = $('#idkecamatan').val();

			if (kotaVal != null || kecamatanVal != null) {
				location.href = window.location.href;
			} else {
				$('#idkota').prop('disabled', true);
				$('#idkecamatan').prop('disabled', true);
			}

		}
	});

	$('#idkota').on('change', function () {
		let provinsi = $('#idkota').val();

		if (provinsi != null) {
			$('#idkecamatan').removeAttr('disabled');
		} else {
			let kecamatanVal = $('#idkecamatan').val();

			if (kecamatanVal != null) {
				location.href = window.location.href;
			} else {
				$('#idkecamatan').prop('disabled', true);
			}
		}
	});


	$('#idprovinsi').select2({
		minimumInputLength: 3,
		allowClear: true,
		placeholder: 'Masukkan Provinsi',
		ajax: {
			dataType: 'json',
			type: 'POST',
			url: "<?= base_url('profil/getProvince') ?>",
			delay: 250,
			data: function (params) {
				return {
					cari: params.term
				}
			},
			processResults: function (data, page) {
				return {
					results: data
				};
			},
		}
	});

	$('#idkota').select2({
		minimumInputLength: 3,
		allowClear: true,
		placeholder: 'Masukkan Kota/Kabupaten',
		ajax: {
			dataType: 'json',
			type: 'POST',
			url: "<?= base_url('profil/getCity') ?>",
			delay: 250,
			data: function (params) {
				return {
					cari: params.term,
					idprovinsi: $('#idprovinsi').val()
				}
			},
			processResults: function (data, page) {
				return {
					results: data
				};
			},
		}
	});

	$('#idkecamatan').select2({
		minimumInputLength: 3,
		allowClear: true,
		placeholder: 'Masukkan Kecamatan',
		ajax: {
			dataType: 'json',
			type: 'POST',
			url: "<?= base_url('profil/getSubdistrict') ?>",
			delay: 250,
			data: function (params) {
				return {
					cari: params.term,
					idkota: $('#idkota').val()
				}
			},
			processResults: function (data, page) {
				return {
					results: data
				};
			},
		}
	});

</script>
