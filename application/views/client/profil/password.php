<div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
        <?= $this->session->flashdata('message') ?>
        <form action="<?= base_url('profil/password') ?>" method="POST">
            <div class="form-group">
                <label for="oldpassword">Password Sekarang</label>
                <input type="password" class="form-control" name="oldpassword" id="oldpassword" value="">
            </div>
            <div class="form-group">
                <label for="oldpassword">Password Baru</label>
                <input type="password" class="form-control" name="newpassword" id="newpassword" value="">
            </div>
            <div class="form-group">
                <label for="oldpassword">Konfirmasi Password Baru</label>
                <input type="password" class="form-control" name="newpasswordconf" id="newpasswordconf" value="">
            </div>
            <button type="submit" data-type="save" class="btn btn-primary py-3 px-4">Simpan</button>
        </form>
    </div>
    <div class="col-md-2">
    </div>
</div>