<?php
	$name = getUserDetails()['nama'];
	(array) $name = explode(" ", $name);
	$firstName = $name[0];

	$cart = $this->session->userdata('cart');
	$favorite = $this->session->userdata('favorite');

?>

<body class="goto-here">
	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
		<div class="container">
			<a class="navbar-brand" href="<?= base_url() ?>"><?= settingSIM()['set_appname'] ?></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
				aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="oi oi-menu"></span>
			</button>

			<div class="collapse navbar-collapse" id="ftco-nav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item <?= $active == 'beranda' ? 'active' : '' ?>"><a href="<?= base_url() ?>"
							class="nav-link">Beranda</a></li>
					<li class="nav-item <?= $active == 'produk' ? 'active' : '' ?>"><a href="<?= base_url('produk') ?>"
							class="nav-link">Produk</a></li>
					<li class="nav-item <?= $active == 'tentang' ? 'active' : '' ?>"><a
							href="<?= base_url('home/tentang') ?>" class="nav-link">Tentang</a></li>
					<?php if (!$this->session->userdata('token')) : ?>
					<li class="nav-item"><a href="<?= base_url('auth') ?>" class="nav-link">Login</a></li>
					<li class="nav-item"><a href="<?= base_url('auth/registrasi') ?>"
							class="nav-link badge-success text-white pl-3">Daftar</a></li>
					<?php else : ?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle <?= $active == 'profil' ? 'active' : '' ?>" href="#"
							id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Halo,
							<?= $firstName; ?></a>
						<div class="dropdown-menu" aria-labelledby="dropdown05">
							<?php if (in_array($this->session->userdata['idrole'], ['role_admin', 'admin'])) { ?>
							<a class="dropdown-item" href="<?= site_url('home') ?>">Admin Panel</a>
							<?php } ?>
							<a class="dropdown-item" href="<?= site_url('profil') ?>">Profil</a>
							<a class="dropdown-item" href="<?= site_url('transaksi/pesanansaya') ?>">Pesanan Saya</a>
							<a class="dropdown-item" href="<?= site_url('auth/logout') ?>">Logout</a>
						</div>
					</li>
					<li class="nav-item cta cta-colored"><a href="<?= base_url('keranjang') ?>" class="nav-link"
							id="cart-num"><i class="icon-shopping_cart"></i><?= getTotalItemCart(); ?></a>
					</li>
					<li class="nav-item cta cta-colored"><a href="<?= base_url('beranda/wishlist') ?>"
							class="nav-link"><i class="icon-favorite"></i><?= getWishList()->num_rows(); ?></a>
					</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</nav>
	<!-- END nav -->
