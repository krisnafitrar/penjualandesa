<!DOCTYPE html>
<html lang="en">

<head>
	<title><?= $title; ?> - <?= settingSIM()['set_appname'] ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="<?= base_url('assets/v2/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap"
		rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="<?= base_url('assets/client/css/open-iconic-bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/client/css/animate.css') ?>">

	<link rel="stylesheet" href="<?= base_url('assets/client/css/owl.carousel.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/client/css/owl.theme.default.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/client/css/magnific-popup.css') ?>">

	<link rel="stylesheet" href="<?= base_url('assets/client/css/aos.css') ?>">

	<link rel="stylesheet" href="<?= base_url('assets/client/css/ionicons.min.css') ?>">

	<link rel="stylesheet" href="<?= base_url('assets/client/css/bootstrap-datepicker.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/client/css/jquery.timepicker.css') ?>">

	<link rel="stylesheet" href="<?= base_url('assets/w3css/w3.css') ?>">

	<link rel="stylesheet" href="<?= base_url('assets/client/css/flaticon.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/client/css/icomoon.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/client/css/style.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/sweetalert2/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/datatables110/datatables.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/select2/dist/css/select2.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/datepicker/datepicker3.css">


	<script src="<?= base_url('assets/v2/'); ?>vendor/jquery/jquery.min.js"></script>
	<!-- <script src="<?= base_url('assets/v2/'); ?>vendor/jquery-ui/jquery-ui.min.js"></script> -->
	<script src="<?= base_url('assets/v2/'); ?>js/jquery.ajax.js"></script>
	<script src="<?= base_url('assets/v2/'); ?>vendor/select2/dist/js/select2.full.min.js"></script>
	<script src="<?= base_url('assets/v2/'); ?>vendor/sweetalert2/dist/sweetalert2.min.js"></script>
	<script src="<?= base_url('assets/v2/'); ?>vendor/datatables110/datatables.min.js"></script>
	<script src="<?= base_url('assets/v2/'); ?>vendor/datepicker/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url('assets/v2/') ?>vendor/tinymce/tinymce.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
	<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js"
		data-client-key="<?= config_item('client_key') ?>"></script>


</head>
