<?php

function setting($key)
{
    $CI = get_instance();
    return $CI->db->get_where('setting', ['kode' => $key])->row_array()['isi'];
}

?>
<footer class="ftco-footer ftco-section">
	<div class="container">
		<div class="row">
			<div class="mouse">
				<a href="#" class="mouse-icon">
					<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
				</a>
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-md">
				<div class="ftco-footer-widget mb-4">
					<h2 class="ftco-heading-2"><?= settingSIM()['set_appname'] ?></h2>
					<p>Temukan produk terbaik dari berbagai desa.</p>
					<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
						<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
						<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
						<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md">
				<div class="ftco-footer-widget mb-4">
					<h2 class="ftco-heading-2">Ada Pertanyaan?</h2>
					<div class="block-23 mb-3">
						<ul>
							<li class="mb-3"><span class="icon icon-map-marker"></span><span
									class="text"><?= setting('set_address'); ?></span></li>
							<li><a href="#"><span class="icon icon-phone"></span><span
										class="text"><?= setting('set_callcenter'); ?></span></a></li>
							<li><a href="#"><span class="icon icon-envelope"></span><span
										class="text"><?= setting('set_emailcenter'); ?></span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">

				<p>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;
					<script>
						document.write(new Date().getFullYear());

					</script> All rights reserved | <a href="https://adalfian.site">Ada & Kfr Project</a>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>
			</div>
		</div>
	</div>
</footer>



<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
		<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
		<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
			stroke="#F96D00" /></svg></div>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/client/js/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/v2/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/v2/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="<?= base_url('assets/client/js/jquery-migrate-3.0.1.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.easing.1.3.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.waypoints.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.stellar.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/owl.carousel.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.magnific-popup.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/aos.js') ?>"></script>
<script src="<?= base_url('assets/client/js/jquery.animateNumber.min.js') ?>"></script>
<script src="<?= base_url('assets/client/js/bootstrap-datepicker.js') ?>"></script>
<script src="<?= base_url('assets/client/js/scrollax.min.js') ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="<?= base_url('assets/client/js/main.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
</body>

</html>
