<?php if (!$paysuccess) { ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="<?= $client_key ?>">
</script>
<section class="ftco-section">
	<div class="container">
		<center>
			<h4>Payment Page</h4>
		</center>
	</div>
</section>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function (event) {
		snap.pay('<?= $token ?>');
	});

</script>
<?php } else { ?>
<section class="ftco-section">
	<div class="container">
		<h1>Payment Success</h1>
	</div>
</section>
<?php } ?>
