<section class="ftco-section">
	<div class="container">
		<div class="row mt-3">
			<?= $this->session->flashdata('message') ?>
			<?php if (count($wishlist) > 0) : ?>
			<?php foreach($wishlist as $p):?>
			<div class="col-6 col-md-4 ftco-animate">
				<a href="<?= base_url('produk/detail/' . $p['slug']) ?>">
					<div class="card card-fluid mb-2 mt-2">
						<img class="card-img-top" style="height: 200px;"
							src="<?= base_url('assets/img/produk/') . $p['foto'] ?>" alt="Card image cap">
						<div class="card-body">
							<span class="card-title text-dark"><?= $p['namaproduk'] ?></span>
							<p class="card-text text-primary"><?= toRupiah($p['harga']) ?></p>
							<i class="fas fa-map-marked-alt mr-1"></i><small><?= $p['namadesa'] ?>,
								<?= $p['kota'] ?></small>
							<div class="row mt-2">
								<button type="button" data-type="btn-cart" style="font-size: 11px; margin-left: 5px;"
									data-id="<?= $p['idproduk'] ?>" class="btn btn-primary btn-block mr-2">Tambah ke
									keranjang<i class="fas fa-cart-plus ml-1"></i></button>
							</div>
							<div class="row mt-2">
								<button type="button" data-type="btn-fav" style="font-size: 11px; margin-left: 5px;"
									data-id="<?= $p['idproduk'] ?>" class="btn btn-danger btn-block mr-2">Hapus
									dari
									wishlist<i class="fas fa-heart ml-1"></i></button>
							</div>
						</div>
					</div>
				</a>
			</div>
			<?php endforeach;?>
			<?php else : ?>
			<div class="col-lg-12 mt-3">
				<div class="text-center">
					<img src="<?= base_url('assets/img/illustration/favorite.png') ?>" class="img-fluid img-produk"
						width="500px" height="300px" alt="">
					<h3 class="text-secondary mt-3"><b>Wishlist Produk Kosong</b></h3>
					<p>Hmm kamu belum menentukan produk favorit kamu, ayo segera tentukan.</p>
					<a href="<?= base_url('produk') ?>" class="btn btn-primary">Cari Produk Favorit</a>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>

<script>
	$('[data-type=btn-cart]').click(function () {
		var idproduk = $(this).attr('data-id');
		var idpembeli = "<?= getPembeli()->row_array()['idpembeli'] ?>";
		var act = 'add';

		$.ajax({
			url: "<?= base_url('keranjang/cart') ?>",
			type: 'post',
			data: {
				idproduk: idproduk,
				idpembeli: idpembeli,
				jumlah: 1,
				act: act
			},
			success: function () {
				document.location.href = "<?= base_url('beranda/wishlist') ?>";
			}
		});

	});

	$('[data-type=btn-fav]').click(function () {
		var idproduk = $(this).attr('data-id');
		var idpembeli = "<?= getPembeli()->row_array()['idpembeli'] ?>";
		var act = 'del';

		$.ajax({
			url: "<?= base_url('produk/wishlist') ?>",
			type: 'post',
			data: {
				idproduk: idproduk,
				idpembeli: idpembeli,
				act: act
			},
			success: function () {
				document.location.href = "<?= base_url('beranda/wishlist') ?>";
			}
		});

	});

</script>
