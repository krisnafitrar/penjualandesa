<?php
	$sess = $this->session->userdata('email');
?>

<section id="home-section" class="hero">
	<div class="home-slider owl-carousel">
		<div class="slider-item" style="background-image: url(<?= base_url('assets/img/jumbotron/kerajinan.jpg') ?>);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

					<div class="col-md-12 ftco-animate text-center">
						<h1 class="mb-2">Produk unggulan desa</h1>
						<h2 class="subheading mb-4">Memasarkan produk terbaik dari berbagai desa</h2>
						<p><a href="<?= base_url('produk') ?>" class="btn btn-primary">Lihat Detail</a></p>
					</div>

				</div>
			</div>
		</div>

		<div class="slider-item" style="background-image: url(<?= base_url('assets/img/jumbotron/kerajinan2.jpg') ?>);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

					<div class="col-sm-12 ftco-animate text-center">
						<h1 class="mb-2">100% Produk asli olahan desa</h1>
						<h2 class="subheading mb-4">100% produk yang dihasilkan berasal dari desa</h2>
						<p><a href="<?= base_url('produk') ?>" class="btn btn-primary">Lihat Detail</a></p>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<section class="ftco-section">
	<div class="container">
		<div class="row no-gutters ftco-services">
			<div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
				<div class="media block-6 services mb-md-0 mb-4">
					<div class="icon bg-color-1 active d-flex justify-content-center align-items-center mb-2">
						<span class="flaticon-shipped"></span>
					</div>
					<div class="media-body">
						<h3 class="heading">Ongkir Murah</h3>
						<span>Saat anda membeli barang</span>
					</div>
				</div>
			</div>
			<div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
				<div class="media block-6 services mb-md-0 mb-4">
					<div class="icon bg-color-3 d-flex justify-content-center align-items-center mb-2">
						<span class="flaticon-award"></span>
					</div>
					<div class="media-body">
						<h3 class="heading">Kualitas Terbaik</h3>
						<span>Kualitas terbaik setiap produk</span>
					</div>
				</div>
			</div>
			<div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
				<div class="media block-6 services mb-md-0 mb-4">
					<div class="icon bg-color-4 d-flex justify-content-center align-items-center mb-2">
						<span class="flaticon-customer-service"></span>
					</div>
					<div class="media-body">
						<h3 class="heading">Customer Service</h3>
						<span>24/7 Service</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="ftco-section">
	<div class="container">
		<div class="row justify-content-center mb-3 pb-3">
			<div class="col-md-12 heading-section text-center ftco-animate">
				<span class="subheading">Produk Desa Terbaik</span>
				<h3 class="mb-4">Produk Terfavorit</h3>
				<p>Berikut ini adalah produk desa terfavorit, apakah anda suka?</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php foreach($produk as $p):?>
			<div class="col-6 col-md-3 ftco-animate">
				<a href="<?= base_url('produk/detail/' . $p['slug']) ?>">
					<div class="card card-fluid mb-2 mt-2">
						<img class="card-img-top" style="height: 200px;"
							src="<?= base_url('assets/img/produk/') . $p['foto'] ?>" alt="Card image cap">
						<div class="card-body">
							<span class="card-title text-dark"><?= $p['namaproduk'] ?></span>
							<p class="card-text text-primary"><?= toRupiah($p['harga']) ?></p>
							<i class="fas fa-map-marked-alt mr-1"></i><small><?= $p['namadesa'] ?>,
								<?= $p['kota'] ?></small>
							<?php if($sess): ?>
							<div class="row mt-2">
								<button type="button" data-type="btn-cart" style="font-size: 11px; margin-left: 5px;"
									data-id="<?= $p['idproduk'] ?>" class="btn btn-primary btn-block mr-2">Tambah ke
									keranjang<i class="fas fa-cart-plus ml-1"></i></button>
							</div>
							<div class="row mt-2">
								<?php if($p['idfavorit'] == null):  ?>
								<button type="button" data-type="btn-fav" style="font-size: 11px; margin-left: 5px;"
									data-id="<?= $p['idproduk'] ?>" data-act="add"
									class="btn btn-secondary btn-block mr-2">Tambah
									ke
									wishlist<i class="fas fa-heart ml-1"></i></button>
								<?php else:?>
								<button type="button" data-type="btn-fav" data-id="<?= $p['idproduk'] ?>" data-act="del"
									style="font-size: 11px; margin-left: 5px;"
									class="btn btn-danger btn-block mr-2">Hapus dari
									wishlist<i class="fas fa-trash ml-1"></i></button>
								<?php endif;?>
							</div>
							<?php endif;?>
						</div>
					</div>
				</a>
			</div>
			<?php endforeach;?>
		</div>
		<div class="row">
			<div class="col-lg-12 d-flex justify-content-center">
				<a href="<?= base_url('produk') ?>" class="btn btn-outline-success mt-3">Lihat
					produk lainnya</a>
			</div>
		</div>
	</div>
</section>

<hr>
<script>
	$('[data-type=btn-cart]').click(function () {
		var idproduk = $(this).attr('data-id');
		var idpembeli = "<?= $pembeli['idpembeli'] ?>";
		var act = 'add';

		$.ajax({
			url: "<?= base_url('keranjang/cart') ?>",
			type: 'post',
			data: {
				idproduk: idproduk,
				idpembeli: idpembeli,
				jumlah: 1,
				act: act
			},
			success: function () {
				document.location.href = "<?= base_url('beranda') ?>";
			}
		});

	});

	$('[data-type=btn-fav]').click(function () {
		var idproduk = $(this).attr('data-id');
		var idpembeli = "<?= $pembeli['idpembeli'] ?>";
		var act = $(this).attr('data-act');

		$.ajax({
			url: "<?= base_url('produk/wishlist') ?>",
			type: 'post',
			data: {
				idproduk: idproduk,
				idpembeli: idpembeli,
				act: act
			},
			success: function () {
				document.location.href = "<?= base_url('beranda') ?>";
			}
		});

	});

</script>
