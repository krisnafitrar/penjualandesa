<div class="container mt-5 mb-5">
	<?php if($pesanan):?>
	<div class="row">
		<div class="col-md-12 col-sm-12 ftco-animate">
			<div class="cart-list">
				<table class="table">
					<thead class="thead-primary">
						<tr class="text-center">
							<?php foreach ($a_kolom as $col) { ?>
							<th><?= $col['label'] ?></th>
							<?php } ?>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php if (count($pesanan) > 0) { ?>
						<?php
                        $no = 0;
                        foreach ($pesanan as $row) { ?>
						<tr>
							<?php foreach ($a_kolom as $col) { ?>
							<?php if ($col['kolom'] == 'no') { ?>
							<td width="10px"><?= ++$no ?></td>
							<?php } else if ($col['type'] == 'S') { ?>
							<td><?= $col['option'][$row[$col['kolom']]] ?></td>
							<?php } else if ($col['type'] == 'N') { ?>
							<td><?= toRupiah($row[$col['kolom']] + $row['ongkir']) ?></td>
							<?php } else if ($col['type'] == 'D') { ?>
							<td><?= $row[$col['kolom']];  ?></td>
							<?php } else { ?>
							<td><?= $row[$col['kolom']] ?></td>
							<?php } ?>
							<?php } ?>
							<td align="center">
								<a href="<?= site_url('transaksi/detailpesanan/' . $row['idpembayaran']) ?>"
									class="btn btn-warning btn-sm" data-toggle="tooltip" title="Detail"><i
										class="fa fa-eye"></i></a>
								<?php if($row['status'] < 1): ?>
								<button type="button" id="btn-pay" class="btn btn-primary btn-sm"
									data-token="<?= $row['payment_token']? $row['payment_token'] : '' ?>"
									data-toggle="tooltip" title="Bayar"><i class="fa fa-credit-card"></i></button>
								<?php endif;?>
							</td>
						</tr>
						<?php } ?>
						<?php } else { ?>
						<tr class="table table-warning">
							<td colspan="<?= count($a_kolom) + 1 ?>" align="center">Tidak ada pesanan</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php else:?>
<div class="col-lg-12 mt-3" style="margin-bottom: 150px;">
	<div class="text-center">
		<img src="<?= base_url('assets/img/illustration/checklist.png') ?>" class="img-fluid img-produk" width="500px"
			height="300px" alt="">
		<h3 class="text-secondary mt-3"><b>Tidak Ada Pesanan</b></h3>
		<p>Hmm kamu belum pernah belanja di <?= settingSIM()['set_appname'] ?>, yuk belanja sekarang.</p>
		<a href="<?= base_url('produk') ?>" class="btn btn-primary">Belanja Sekarang</a>
	</div>
</div>
<?php endif;?>
<script>
	let btnPay = document.getElementById('btn-pay');
	let token = btnPay.getAttribute('data-token');

	btnPay.addEventListener('click', function () {
		snap.pay(token);
	});

</script>
