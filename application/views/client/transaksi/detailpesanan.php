<div class="container mb-5">
	<div class="row">
		<div class="col-md-12 ftco-animate">
			<div class="card shadow mb-4">
				<div class="card-body">
					<div class="row">
						<table class="ml-3 mb-3">
							<tr>
								<td width="150px">
									<span><b>Invoice</b></span>
								</td>
								<td width="20px">
									<span>:</span>
								</td>
								<td>
									<span><?= $detail[0]['invoice'] ?></span>
								</td>
							</tr>
							<tr>
								<td width="150px">
									<span><b>Penerima</b></span>
								</td>
								<td width="20px">
									<span>:</span>
								</td>
								<td>
									<span><?= $detail[0]['penerima'] ?></span>
								</td>
							</tr>
							<tr>
								<td width="150px">
									<span><b>Tgl Pesan</b></span>
								</td>
								<td width="20px">
									<span>:</span>
								</td>
								<td>
									<span><?= $detail[0]['time'] ?></span>
								</td>
							</tr>
							<tr>
								<td width="150px">
									<span><b>Status</b></span>
								</td>
								<td width="20px">
									<span>:</span>
								</td>
								<td>
									<span><?= $detail[0]['status_pembayaran'] == 0 ? 'Belum bayar' : 'Sudah bayar' ?></span>
								</td>
							</tr>
							<tr>
								<td width="150px">
									<span><b>Alamat</b></span>
								</td>
								<td width="20px">
									<span>:</span>
								</td>
								<td>
									<span><?= $detail[0]['alamat'] ?></span>
								</td>
							</tr>
						</table>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?= $this->session->flashdata('message'); ?>
							<form action="<?= site_url('master/kategori') ?>" method="post" id="form-list">
								<div class="cart-list">
									<table class="w3-table-all" id="datatable">
										<thead>
											<tr>
												<th scope="col">Nama Produk</th>
												<th scope="col">Harga</th>
												<th scope="col">Qty</th>
												<th scope="col">Total</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1;
                                        $jumlah = 0;
                                        $grand_total = 0;
                                        ?>
											<?php foreach ($detail as $d) : ?>
											<tr>
												<td><?= $d['namaproduk'] ?></td>
												<td><?= toRupiah($d['harga']) ?></td>
												<td><?= $d['jumlah'] ?> item</td>
												<td><?= toRupiah($d['jumlah'] * $d['harga']) ?></td>
												<?php $jumlah = $d['jumlah'] * $d['harga'] ?>
											</tr>
											<?php $i++; ?>
											<?php $grand_total += $jumlah ?>
											<?php endforeach; ?>
											<tr class="table-secondary">
												<td colspan="3" class="w3-right-align"><b>Total Estimasi</b></td>
												<td><?= toRupiah($grand_total); ?></td>
											</tr>
											<tr class="table-secondary">
												<td colspan="3" class="w3-right-align"><b>Ongkir</b></td>
												<td><?= toRupiah($d['delivery_cost']); ?></td>
											</tr>
											<tr class="table-secondary">
												<td colspan="3" class="w3-right-align"><b>Grand Total</b></td>
												<td><?= toRupiah($grand_total + $d['delivery_cost']); ?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<input type="hidden" name="act" id="act">
								<input type="hidden" name="key" id="key">
							</form>
						</div>
					</div>
				</div>
			</div>
			<a href="<?= base_url('transaksi/pesanansaya') ?>" class="btn btn-primary">Kembali ke halaman pesanan
				saya</a>
		</div>

	</div>
</div>
<!-- End of Main Content -->
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">Upload Bukti Pembayaran</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?= base_url('transaksi/uploadbukti') ?>" method="POST" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="form-group">
						<label for="buktibayar">Bukti Pembayaran</label>
						<input type="file" name="buktibayar" id="buktibayar" class="form-control" required>
					</div>
					<input type="hidden" value="<?= $detail[0]['idpembayaran'] ?>" name="idpembayaran">
					<input type="hidden" value="<?= $detail[0]['buktibayar'] ?>" name="oldbukti">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Upload <i class="fa fa-upload ml-1"></i></button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	$('[data-type=btn-upload]').click(function () {
		var modal = $('#exampleModalCenter');
		modal.modal();
	});

</script>
