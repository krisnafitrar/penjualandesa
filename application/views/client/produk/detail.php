<style>
	.table1,
	th,
	td {
		padding: 8px 40px;
	}

</style>
<?php
	$session = $this->session->userdata('email');
?>
<div class="container mt-5 mb-5">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb bg-transparent">
			<li class="breadcrumb-item"><a href="<?= base_url('produk') ?>">Produk</a></li>
			<li class="breadcrumb-item active" aria-current="page">Detail Produk</li>
		</ol>
	</nav>
	<div class="row">
		<div class="col-lg-6 mb-5 ftco-animate">
			<a href="<?= base_url('assets/img/produk/') . $produk['foto'] ?>" class="image-popup"><img
					src="<?= base_url('assets/img/produk/') . $produk['foto'] ?>" class="img-fluid"
					style="border-radius: 15px;" alt=""></a>
		</div>
		<div class="col-lg-6 product-details pl-md-5 ftco-animate">
			<h3><?= $produk['namaproduk'] ?></h3>
			<div class="rating d-flex">
				<p class="text-left mr-4">
					<?= $produk['namajenisproduk'] ?>
				</p>
			</div>
			<p class="price"><span><?= toRupiah($produk['harga']) ?></span></p>
			<div class="row mt-4">
				<div class="col-md-6 mb-2">
					<span><i
							class="fas fa-map-marked-alt mr-2"></i><?= $produk['namadesa'] . ', ' .$produk['kota'] ?></span><br>
					<span><i class="fas fa-cubes mr-2"></i> <?= $produk['stok'] ?> tersisa</span>
				</div>
				<div class="w-100"></div>
				<div class="input-group col-md-6 d-flex mb-3">
					<input type="number" id="quantity" name="quantity" class="form-control input-number" value="1"
						min="1" max="10">
				</div>
				<div class="w-100"></div>
			</div>
			<p><a href="<?= !$session ? base_url('auth'): '#' ?>" data-type="<?= $session ? 'btn-add' : '' ?>"
					class="btn btn-black py-3 px-5 text-white"><i class="fas fa-cart-plus mr-2"></i>
					Add to
					Cart</a></p>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<h5>Spesifikasi Produk</h5>
					<table class="table1" style="margin-left: -40px;">
						<tr>
							<td>Kategori</td>
							<td class="text-dark"><?= $produk['namakategori'] ?></td>
						</tr>
						<tr>
							<td>Merek</td>
							<td class="text-dark">Coba</td>
						</tr>
						<tr>
							<td>Asal Produk</td>
							<td class="text-dark"><?= $produk['namadesa'] . ', ' .$produk['kota'] ?></td>
						</tr>
						<tr>
							<td>Ukuran</td>
							<td class="text-dark">5x3</td>
						</tr>
						<tr>
							<td>Bahan</td>
							<td class="text-dark"><?= $produk['bahan'] ?></td>
						</tr>
						<tr>
							<td>Stok</td>
							<td class="text-dark"><?= $produk['stok'] ?></td>
						</tr>
						<tr>
							<td>Berat</td>
							<td class="text-dark"><?= $produk['berat'] ?> kg</td>
						</tr>
					</table>
					<h5 class="mt-3">Deskripsi Produk</h5>
					<p>
						<?= $produk['deskripsi'] ?>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$('[data-type=btn-add]').click(function () {
		var idproduk = "<?= $produk['idproduk'] ?>";
		var slug = "<?= $produk['slug'] ?>";
		var jumlah = $('#quantity').val();
		var idpembeli = "<?= getPembeli()->row_array()['idpembeli'] ?>";
		var act = 'add';

		$.ajax({
			url: "<?= base_url('keranjang/cart') ?>",
			type: 'post',
			data: {
				idproduk: idproduk,
				idpembeli: idpembeli,
				jumlah: jumlah,
				act: act
			},
			success: function () {
				document.location.href = "<?= base_url('produk/detail/') ?>" + slug;
			}
		});

	});

</script>
