<?php
	$sess = $this->session->userdata('email');
	$kategori_uri = $_GET['kategori'];
?>
<section class="ftco-section" style="transform: translateY(-40px);">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-sm-0 col-md-2"><input type="hidden"></div>
			<div class="col-lg-10 col-md-10 col-sm-5">
				<div class="row mb-3">
					<div class="col-9 col-lg-8">
						<input type="text" name="keyword" id="keyword" class="w3-input w3-border w3-round-large"
							placeholder=" Mau belanja apa ..." value="<?= $_GET['keyword'] ? $_GET['keyword'] : '' ?>">
					</div>
					<div class="col-3 col-lg-3">
						<?php if (!empty($_GET['keyword'])) { ?>
						<span style="cursor: pointer;" data-type="btn-clear-search"><i class="fas fa-times"
								style="transform: translateX(-20px) translateY(5px);"></i></span>
						<?php } ?>
						<button type="button" data-type="btn-search" class="btn btn-primary mt-1">Search</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-2 d-none d-lg-block" style="transform: translateY(-55px);">
				<h6 class="mb-3">Kategori</h6>
				<div class="nav flex-column nav-pills mb-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="nav-link <?= $kategori_uri == null ? 'active':'' ?>" id="v-pills-home-tab"
						href="<?= base_url('produk') ?>" style="font-size: 13px;">Semua</a>
					<?php foreach($kategori as $k):?>
					<?php $isActive = $kategori_uri == $k['slug_kategori']; ?>
					<a class="nav-link <?= $isActive ? 'active':'' ?>" id="v-pills-home-tab"
						href="<?= base_url('produk?kategori=' . $k['slug_kategori']) ?>"
						style="font-size: 13px"><?= $k['namakategori'] ?></a>
					<?php endforeach;?>
				</div>
			</div>
			<div class="col-lg-10 col-md-10 col-sm-12">
				<div class="row">
					<div class="col-6 col-lg-4 d-md-none">
						<select name="kategori" id="kategori" class="w3-select w3-border w3-round-large"
							style="height: 30px; font-size: 14px; padding: 3px;">
							<option value="all">-- Filter Kategori --</option>
							<option value="all">Semua Kategori</option>
							<?php foreach($kategori as $k):?>
							<option value="<?= $k['slug_kategori'] ?>"
								<?= $kategori_uri == $k['slug_kategori'] ? 'selected':'' ?>>
								<?= $k['namakategori'] ?>
							</option>
							<?php endforeach;?>
						</select>
					</div>
					<?php if($produk):?>
					<div class="col-6 col-lg-4">
						<select name="sort" id="sort" class="w3-select w3-border w3-round-large"
							style="height: 30px; font-size: 14px; padding: 3px;">
							<option value="">-- Urut berdasarkan --</option>
							<option value="0" <?= $_GET['sort'] == 0 ? 'selected':'' ?>>Relevansi
							</option>
							<option value="1" <?= $_GET['sort'] == 1 ? 'selected':'' ?>>Harga Terendah
							</option>
							<option value="2" <?= $_GET['sort'] == 2 ? 'selected':'' ?>>Harga
								Tertinggi</option>
						</select>
					</div>
				</div>
				<div class="row mt-2">
					<?php if($_GET['keyword']): ?>
					<span class="mb-2 ml-3" style="font-size: 13px;"><?= count($produk) ?> produk
						"<b><?= $_GET['keyword'] ?></b>" di <?= $_GET['kategori'] ? str_replace('-', ' ', $_GET['kategori']) :'semua
						kategori' ?></span>
					<?php else:?>
					<span class="mb-2 ml-3" style="font-size: 13px;"><?= count($produk) ?> produk ditemukan</span>
					<?php endif;?>
				</div>
				<div class="row">
					<?php foreach($produk as $p):?>
					<div class="col-6 col-lg-4 ftco-animate">
						<div class="card card-fluid mb-2 mt-2">
							<a href="<?= base_url('produk/detail/' . $p['slug']) ?>">
								<img class="card-img-top" style="height: 200px;"
									src="<?= base_url('assets/img/produk/') . $p['foto'] ?>" alt="Card image cap">
							</a>
							<div class="card-body">
								<a href="<?= base_url('produk/detail/' . $p['slug']) ?>"
									class="card-title text-dark"><?= $p['namaproduk'] ?></a>
								<p class="card-text text-primary"><?= toRupiah($p['harga']) ?></p>
								<i class="fas fa-map-marked-alt mr-1"></i><small><?= $p['namadesa'] ?>,
									<?= $p['kota'] ?></small>
								<?php if($sess): ?>
								<div class="row mt-2">
									<button type="button" data-type="btn-cart"
										style="font-size: 11px; margin-left: 5px;" data-id="<?= $p['idproduk'] ?>"
										class="btn btn-primary btn-block mr-2">Tambah ke
										keranjang<i class="fas fa-cart-plus ml-1"></i></button>
								</div>
								<div class="row mt-2">
									<?php if($p['idfavorit'] == null):  ?>
									<button type="button" data-type="btn-fav" style="font-size: 11px; margin-left: 5px;"
										data-id="<?= $p['idproduk'] ?>" data-act="add"
										class="btn btn-secondary btn-block mr-2">Tambah
										ke
										wishlist<i class="fas fa-heart ml-1"></i></button>
									<?php else:?>
									<button type="button" data-type="btn-fav" data-id="<?= $p['idproduk'] ?>"
										data-act="del" style="font-size: 11px; margin-left: 5px;"
										class="btn btn-danger btn-block mr-2">Hapus dari
										wishlist<i class="fas fa-trash ml-1"></i></button>
									<?php endif;?>
								</div>
								<?php endif;?>
							</div>
						</div>
					</div>
					<?php endforeach;?>
					<?php else : ?>
					<div class="col-lg-12 mt-3">
						<div class="text-center">
							<img src="<?= base_url('assets/img/illustration/search.png') ?>"
								class="img-fluid img-produk" width="500px" height="300px" alt="">
							<h3 class="text-secondary"><b>Produk tidak ditemukan</b></h3>
							<?php if(!empty($_GET['keyword'])): ?>
							<p>Produk dengan kata kunci "<?= $_GET['keyword'] ?>" tidak ditemukan, coba periksa
								kembali.
							</p>
							<?php else : ?>
							<p>Produk yang kamu cari tidak ditemukan di sistem kami</p>
							<?php endif;?>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	let sess = "<?= $this->session->userdata('email') ?>";


	$('[data-type=btn-cart]').click(function () {
		var idproduk = $(this).attr('data-id');
		var idpembeli = "<?= getPembeli()->row_array()['idpembeli'] ?>";
		var act = 'add';

		$.ajax({
			url: "<?= base_url('keranjang/cart') ?>",
			type: 'post',
			data: {
				idproduk: idproduk,
				idpembeli: idpembeli,
				jumlah: 1,
				act: act
			},
			success: function () {
				document.location.href = "<?= base_url('produk') ?>";
			}
		});

	});

	$('[data-type=btn-fav]').click(function () {
		var idproduk = $(this).attr('data-id');
		var idpembeli = "<?= getPembeli()->row_array()['idpembeli'] ?>";
		var act = $(this).attr('data-act');

		console.log(idproduk, idpembeli, act);

		$.ajax({
			url: "<?= base_url('produk/wishlist') ?>",
			type: 'post',
			data: {
				idproduk: idproduk,
				idpembeli: idpembeli,
				act: act
			},
			success: function () {
				document.location.href = "<?= base_url('produk') ?>";
			}
		});

	});

	$('[data-type=btn-search]').click(function () {
		setURI('keyword');
	});

	$('[data-type=btn-clear-search]').click(function () {
		let url = window.location.href;
		let urlObj = new URL(url);

		let newUrl = url.replace(new RegExp("keyword" + "=\\w+"), "").replace("?&", "?")
			.replace("&&", "&");

		location.href = newUrl;

	});

	$('#sort').on('change', function () {
		setURI('sort');
	});

	$('#kategori').on('change', function () {
		setURI('kategori');
	});



	function setURI(params) {
		let url = window.location.href;
		let value = $('#' + params).val();
		let urlObject = new URL(url);

		if (value != "") {
			if (url.includes("?")) {
				if (!url.includes(params)) {
					url += `&${params}=` + value;
				} else {
					url = url.replace(`${params}=` + urlObject.searchParams.get(params), `${params}=${value}`);
				}
			} else {
				url += `?${params}=` + value;
			}
		}

		if (params == 'kategori' && value == 'all') {
			url = url.replace(new RegExp("kategori" + "=\\w+"), "").replace("?&", "?")
				.replace("&&", "&");
		}

		location.href = url;
	}

</script>
