<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <a class="btn btn-primary mb-3" href="<?= $menu . '/create' ?>">Tambah <?= $title; ?></a>

                <div class="row">
                    <div class="col-md-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message'); ?>
                        <?= $this->session->flashdata('delete'); ?>
                        <form method="post" id="form_list">
                            <table class="table table-hover" id="datatable">
                                <thead>
                                    <tr>
                                        <?php foreach ($a_kolom as $key => $val) {
                                            if ($val['kolom'] == ':no') { ?>
                                                <th scope="col">No</th>
                                            <?php } else { ?>
                                                <?php if (!isset($val['is_tampil']) || $val['is_tampil'] == true) : ?>
                                                    <th scope="col"><?= $val['label'] ?></th>
                                                <?php endif ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($a_data as $row) { ?>
                                        <tr>
                                            <?php
                                            foreach ($a_kolom as $key => $val) {
                                                if ($val['kolom'] == ':no') { ?>
                                                    <td scope="row"><?= $i++ ?></td>
                                                <?php } else if (isset($val['type']) && $val['type'] == 'S') {
                                                    $option = $val['option'];
                                                ?>
                                                    <td>
                                                        <?php if (!isset($val['is_tampil']) || $val['is_tampil'] == true) : ?>
                                                            <?= $option[$row[$val['kolom']]] ?>
                                                        <?php endif; ?>
                                                    </td>
                                                <?php } else { ?>
                                                    <?php if (!isset($val['is_tampil']) || $val['is_tampil'] == true) : ?>
                                                        <td><?= $val['set_currency'] ? toRupiah($row[$val['kolom']]) : $row[$val['kolom']]; ?></td>
                                                    <?php endif; ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <td>
                                                <?php if (isset($a_buttonlinear)) {
                                                    foreach ($a_buttonlinear as $k => $v) { ?>
                                                        <button type="button" data-type="<?= $v['type'] ?>" data-id="<?= $row[$primary]; ?>" class="btn btn-<?= $v['class'] ?>" <?= $v['add'] ?>><?= $v['label'] ?></button>
                                                <?php }
                                                } ?>
                                                <button type="button" data-type="edit" data-id="<?= $row[$primary]; ?>" class="btn btn-sm btn-info">Ubah</button>
                                                <button type="button" data-type="delete" data-id="<?= $row[$primary]; ?>" class="btn btn-sm btn-danger">Hapus</button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                    <?= $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<script>
    $(function() {
        $('[data-type="edit"]').click(function() {
            location.href = '<?= site_url($parent . '/' . $menu . '/detail/') ?>' + $(this).attr('data-id');
        })
        $('[data-type="delete"]').click(function() {
            var id = $(this).attr('data-id');
            bootbox.confirm("Apakah anda ingin menghapus data?", function(result) {
                if (result) {
                    $('#form_list #key').val(id);
                    $('#form_list #act').val('delete');
                    $('#form_list').submit();
                }
            })
        })
    })
</script>