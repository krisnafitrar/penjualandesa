<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/v2/'); ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/v2/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/v2/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/v2/'); ?>js/sb-admin-2.min.js"></script>
</script>

<script>
    $('.custom-input-file').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });


    $('.form-check-input').on('click', function() {
        const menuId = $(this).data('menu');
        const roleId = $(this).data('role');

        $.ajax({
            url: "<?= base_url('master/changeaccess') ?>",
            type: 'post',
            data: {
                menuId: menuId,
                roleId: roleId
            },
            success: function() {
                document.location.href = "<?= base_url('master/roleaccess/') ?>" + roleId;
            }
        });

    });
</script>

</body>

</html>