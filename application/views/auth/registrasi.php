<section class="ftco-section">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-5">
				<div class="card">
					<div class="card-body">
						<h5 class="text-center">Siap Untuk <b>Belanja</b>?</h5>
						<p class="text-center mb-5"><b>Daftar</b> sekarang juga, belanja kemudian</p>
						<?= $this->session->flashdata('message'); ?>
						<form action="<?= base_url('auth/registrasi') ?>" method="POST">
							<div class="form-group">
								<input type="text" name="nama" id="nama" class="w3-input w3-border w3-round-large"
									placeholder="Nama lengkap kamu" value="<?= set_value('nama') ?>" required>
								<?= form_error('nama', '<small class="text-danger">', '</small>') ?>
							</div>
							<div class="form-group">
								<input type="email" name="email" id="email" class="w3-input w3-border w3-round-large"
									placeholder="Email kamu" value="<?= set_value('email') ?>" required>
								<?= form_error('email', '<small class="text-danger">', '</small>') ?>
							</div>
							<div class="form-group">
								<input type="number" name="notelp" id="notelp" class="w3-input w3-border w3-round-large"
									placeholder="Nomor handphone kamu" value="<?= set_value('notelp') ?>" required>
								<?= form_error('notelp', '<small class="text-danger">', '</small>') ?>
							</div>
							<div class="form-group">
								<input type="password" name="password" id="password"
									class="w3-input w3-border w3-round-large" placeholder="Kata sandi" required>
								<?= form_error('password', '<small class="text-danger">', '</small>') ?>
							</div>
							<div class="form-group">
								<input type="password" name="repeatpassword" id="repeatpassword"
									class="w3-input w3-border w3-round-large" placeholder="Ulangi Kata sandi" required>
								<?= form_error('repeatpassword', '<small class="text-danger">', '</small>') ?>
							</div>
							<button type="submit" class="btn btn-primary btn-block">Daftar</button>
							<a href="<?= base_url('auth') ?>" class="d-block text-center mt-3">Udah punya akun?</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
