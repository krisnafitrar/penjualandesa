<section class="ftco-section">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-5">
				<div class="card">
					<div class="card-body">
						<h5 class="text-center">Lanjutin <b>Belanja</b> Yuk!</h5>
						<p class="text-center mb-5"><b>Masuk</b> ke akun kamu untuk lanjut belanja</p>
						<?= $this->session->flashdata('message'); ?>
						<form action="<?= base_url('auth') ?>" method="POST">
							<div class="form-group">
								<input type="email" name="email" id="email" class="w3-input w3-border w3-round-large"
									placeholder="Email kamu" value="<?= set_value('email') ?>">
								<?= form_error('email', '<small class="text-danger">', '</small>') ?>
							</div>
							<div class="form-group">
								<input type="password" name="password" id="password"
									class="w3-input w3-border w3-round-large" placeholder="Password kamu">
								<?= form_error('password', '<small class="text-danger">', '</small>') ?>
							</div>
							<button type="submit" class="btn btn-primary btn-block">Masuk</button>
							<span class="d-block text-center mt-3">Lupa sandi?</span>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
