<div class="content">
    <div class="container-fluid">
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ml-3 mt-3 mr-3">
                            <?= $this->session->flashdata('message') ?>
                        </div>
                        <form action="<?= base_url('laporan') ?>" method="POST">
                            <div class="row mt-3 ml-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="from">Dari tanggal</label>
                                        <input type="date" name="from" id="from" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="to">Sampai tanggal</label>
                                        <input type="date" name="to" id="to" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="type">Tipe Laporan</label>
                                        <select name="type" id="type" class="form-control">
                                            <option value="income">Pendapatan</option>
                                            <option value="sales">Penjualan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ml-4">
                                <button type="submit" class="btn btn-sm btn-primary">Tampilkan</button>
                            </div>
                        </form>
                        <div class="col-md-8">
                            <?php if (!empty($a_info)) : ?>
                                <?php if (!empty($a_data)) : ?>
                                    <form action="<?= base_url('laporan/cetak') ?>" method="POST">
                                        <input type="hidden" name="tipe_laporan" value="<?= $a_info['report_type'] ?>">
                                        <input type="hidden" name="from_date" value="<?= $a_info['from'] ?>">
                                        <input type="hidden" name="to_date" value="<?= $a_info['to'] ?>">
                                        <div class="form-group ml-4">
                                            <?php if (!empty($a_data)) : ?>
                                                <button type="submit" class="btn btn-sm btn-secondary"><i class="fas fa-print mr-1"></i> Cetak</button>
                                            <?php endif; ?>
                                        </div>
                                    </form>
                                    <table class="table">
                                        <tr>
                                            <td><b>Dari Tanggal</b> : <?= $a_info['from'] ?></td>
                                            <td><b>Sampai Tanggal</b> : <?= $a_info['to'] ?></td>
                                            <td><b>Jenis Laporan</b> : <?= $a_info['report_type'] ?></td>
                                        </tr>
                                    </table>
                                <?php else : ?>
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <strong>Data tidak ditemukan!</strong> Coba inputkan tanggal dengan rentang yang berbeda.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <table id="add-row" class="display table table-hover table-bordered" role="grid" aria-describedby="add-row_info">
                                <thead>
                                    <tr>
                                        <th scope="col">#No</th>
                                        <th scope="col">Tanggal</th>
                                        <?php if ($a_tipe == null || $a_tipe == "income") : ?>
                                            <th scope="col">Total Pendapatan</th>
                                        <?php else : ?>
                                            <th scope="col">Kode Produk</th>
                                            <th scope="col">Nama Produk</th>
                                            <th scope="col">Jumlah</th>
                                        <?php endif ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php $all_total = 0; ?>
                                    <?php $all_sales = 0; ?>
                                    <?php if ($a_data != null) : ?>
                                        <?php foreach ($a_data as $p) : ?>
                                            <?php if ($a_tipe == "income") : ?>
                                                <tr role="row" class="odd">
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $p['tgl']; ?></td>
                                                    <td><?= toRupiah($p['jmlpayment'] + $p['jmlongkir']) ?></td>
                                                </tr>
                                                <?php $all_total += $p['jmlpayment'] + $p['jmlongkir']; ?>
                                            <?php else : ?>
                                                <tr role="row" class="odd">
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $p['tgl']; ?></td>
                                                    <td><?= $p['idproduk']; ?></td>
                                                    <td><?= $p['namaproduk']; ?></td>
                                                    <td><?= $p['terjual']; ?></td>
                                                </tr>
                                                <?php $all_sales += $p['terjual']; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif ?>
                                    <?php if ($a_tipe == "income" || $a_tipe == null) : ?>
                                        <tr>
                                            <td colspan="2"><b>Total Seluruh Pendapatan</b></td>
                                            <td><b><?= toRupiah($all_total); ?></b></td>
                                        </tr>
                                    <?php else : ?>
                                        <tr>
                                            <td colspan="4"><b>Total Seluruh Penjualan</b></td>
                                            <td><b><?= $all_sales . ' item'; ?></b></td>
                                        </tr>
                                    <?php endif; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>