<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan <?= $a_info['report_type'] ?></title>
</head>

<style>
    .table1 {
        font-family: sans-serif;
        color: #232323;
        border-collapse: collapse;
    }

    .table1,
    .table1 th,
    .table1 td {
        border: 1px solid #000000;
        padding: 8px 20px;
    }

    .table0,
    .table0 th,
    .table0 td {
        border: 0px;
        padding: 8px 20px;
    }
</style>

<body>
    <h3 align="center">
        <u>LAPORAN <?= strtoupper($a_info['report_type']) ?></u>
    </h3>
    <br>
    <table class="table0" style="margin-left: -45px">
        <tr>
            <td>Jenis Laporan</td>
            <td>:</td>
            <td><?= $a_info['report_type'] ?></td>
        </tr>
        <tr>
            <td>From Date</td>
            <td>:</td>
            <td><?= date('d M Y', strtotime($a_info['from'])) ?></td>
        </tr>
        <tr>
            <td>To Date</td>
            <td>:</td>
            <td><?= date('d M Y', strtotime($a_info['to'])) ?></td>
        </tr>
    </table>
    <br>
    <table class="table1" align="center" width="100%">
        <tr>
            <?php foreach ($a_kolom as $col) : ?>
                <th><?= $col['label'] ?></th>
            <?php endforeach; ?>
        </tr>
        <?php $i = 1;
        $x = 0;
        ?>
        <?php foreach ($repp_val as $result) : ?>
            <tr>
                <?php foreach ($a_kolom as $col) : ?>
                    <?php if ($a_info['report_type'] == 'Pendapatan') : ?>
                        <?php if ($col['kolom'] == ':no') : ?>
                            <td><?= $i++; ?></td>
                        <?php elseif ($col['kolom'] == 'jmlpayment') : ?>
                            <td><?= toRupiah($result[$col['kolom']] + $result['jmlongkir']) ?></td>
                        <?php else : ?>
                            <td><?= $result[$col['kolom']] ?></td>
                        <?php endif; ?>
                    <?php else : ?>
                        <?php if ($col['kolom'] == ':no') : ?>
                            <td><?= $i++ ?></td>
                        <?php else : ?>
                            <td><?= $result[$col['kolom']] ?></td>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tr>
            <?php if ($a_info['report_type'] == 'Pendapatan') : ?>
                <?php $x += $result['jmlpayment'] + $result['jmlongkir'] ?>
            <?php else : ?>
                <?php $x += $result['terjual'] ?>
            <?php endif; ?>
        <?php endforeach; ?>
        <tr>
            <td colspan="<?= count($a_kolom) - 1 ?>"><b>Total Seluruh <?= $a_info['report_type'] ?></b></td>
            <td><b><?= $a_info['report_type'] == 'Pendapatan' ? toRupiah($x) : $x; ?></b></td>
        </tr>
    </table>
    <br>
    <br>
    <table align="right" class="table0" style="margin-right: -50px; margin-top: 150px">
        <tr>
            <td align="center">
                <img src="https://qrickit.com/api/qr.php?d=<?= settingSIM()['url_qrcode'] . md5(settingSIM()['qrcode_key']) ?>&fgdcolor=000000&qrsize=150&t=p&e=m">
            </td>
        </tr>
        <tr>
            <td align="center"><small>Scan QR Code diatas untuk mengecek validitas laporan</small></td>
        </tr>
    </table>

</body>

</html>