<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
</head>

<body>
    <?php if ($is_valid) : ?>
        <h2>Laporan valid</h2>
    <?php else : ?>
        <h2 style="color: red">Laporan Invalid</h2>
    <?php endif; ?>
</body>

</html>