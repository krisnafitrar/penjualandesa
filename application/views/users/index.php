            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $profile; ?></h1>

                    <div class="row">
                        <div class="col-lg-6">
                            <?= $this->session->flashdata('message'); ?>
                        </div>
                    </div>

                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="<?= base_url('assets/img/profile/') . $user['image']; ?>" class="card-img" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h4 class="card-title mb-0"><b><?= $user['name']; ?></b></h4>
                                    <p class="card-text mb-0"><?= convert($user['role_id']); ?></p>
                                    <p class="card-text mb-0"><?= $user['username']; ?></p>
                                    <p class="card-text mb-0"><?= $user['email']; ?></p>
                                    <p class="card-text mb-0"><small class="text-muted">Member sejak <?= date('d F Y', $user['date_created']); ?></small></p>

                                    <?php
                                    function convert($level)
                                    {
                                        $ci = get_instance();
                                        $query = "SELECT `role` FROM `user_role` WHERE id='$level' ";
                                        $result = $ci->db->query($query)->row_array();
                                        if ($result) {
                                            return $result['role'];
                                        }
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->