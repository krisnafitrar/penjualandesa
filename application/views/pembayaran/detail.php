    <!-- Main Content -->
    <div id="content">
    	<!-- Begin Page Content -->
    	<div class="container-fluid">

    		<!-- Page Heading -->
    		<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    		<div class="col-md-12">
    			<div class="card shadow mb-4">
    				<div class="card-body">
    					<div class="row">
    						<table class="ml-3 mb-3">
    							<tr>
    								<td width="150px">
    									<span><b>Invoice</b></span>
    								</td>
    								<td width="20px">
    									<span>:</span>
    								</td>
    								<td>
    									<span><?= $detail[0]['invoice'] ?></span>
    								</td>
    							</tr>
    							<tr>
    								<td width="150px">
    									<span><b>Penerima</b></span>
    								</td>
    								<td width="20px">
    									<span>:</span>
    								</td>
    								<td>
    									<span><?= $detail[0]['penerima'] ?></span>
    								</td>
    							</tr>
    							<tr>
    								<td width="150px">
    									<span><b>Tgl Pesan</b></span>
    								</td>
    								<td width="20px">
    									<span>:</span>
    								</td>
    								<td>
    									<span><?= date('d-M-Y H:i:s', $detail[0]['tgl']) ?></span>
    								</td>
    							</tr>
    							<tr>
    								<td width="150px">
    									<span><b>Status</b></span>
    								</td>
    								<td width="20px">
    									<span>:</span>
    								</td>
    								<td>
    									<span><?= $detail['status'] == 0 ? 'Belum bayar' : 'Sudah bayar' ?></span>
    								</td>
    							</tr>
    							<tr>
    								<td width="150px">
    									<span><b>Alamat</b></span>
    								</td>
    								<td width="20px">
    									<span>:</span>
    								</td>
    								<td>
    									<span><?= $detail[0]['alamat'] ?></span>
    								</td>
    							</tr>
    						</table>
    					</div>
    					<button data-type="update" class="btn btn-success mb-3"><i class="fas fa-edit mr-1"></i> Update
    						status pesanan</button>
    					<div class="row">
    						<div class="col-md-12">
    							<?= $this->session->flashdata('message'); ?>
    							<form action="<?= site_url('master/kategori') ?>" method="post" id="form-list">
    								<table class="table table-hover" id="datatable">
    									<thead>
    										<tr>
    											<th scope="col">ID Produk</th>
    											<th scope="col">Nama Produk</th>
    											<th scope="col">Harga</th>
    											<th scope="col">Jumlah</th>
    											<th scope="col">Desa</th>
    											<th scope="col">Total</th>
    										</tr>
    									</thead>
    									<tbody>
    										<?php $i = 1;
                                            $jumlah = 0;
                                            $grand_total = 0;
                                            ?>
    										<?php foreach ($detail as $d) : ?>
    										<tr>
    											<td><?= $d['idproduk'] ?></td>
    											<td><?= $d['namaproduk'] ?></td>
    											<td><?= toRupiah($d['harga']) ?></td>
    											<td><?= $d['jumlah'] ?></td>
    											<td><?= $d['namadesa'] ?></td>
    											<td><?= toRupiah($d['jumlah'] * $d['harga']) ?></td>
    											<?php $jumlah = $d['jumlah'] * $d['harga'] ?>
    										</tr>
    										<?php $i++; ?>
    										<?php $grand_total += $jumlah ?>
    										<?php endforeach; ?>
    										<tr class="table-primary">
    											<td colspan="5" align="right"><b>Total Estimasi</b></td>
    											<td><?= toRupiah($grand_total); ?></td>
    										</tr>
    										<tr class="table-warning">
    											<td colspan="5" align="right"><b>Ongkir</b></td>
    											<td><?= toRupiah($d['ongkir']); ?></td>
    										</tr>
    										<tr class="table-info">
    											<td colspan="5" align="right"><b>Grand Total</b></td>
    											<td><?= toRupiah($grand_total + $d['ongkir']); ?></td>
    										</tr>
    									</tbody>
    								</table>

    								<input type="hidden" name="act" id="act">
    								<input type="hidden" name="key" id="key">
    							</form>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>


    	</div>
    	<!-- /.container-fluid -->

    </div>

    <!-- End of Main Content -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
    	aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title" id="exampleModalCenterTitle">Update Status Pesanan</h5>
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    					<span aria-hidden="true">&times;</span>
    				</button>
    			</div>
    			<form action="<?= base_url('pembayaran/detail/' . $detail[0]['idpembayaran']) ?>" method="POST">
    				<div class="modal-body">
    					<div class="form-group">
    						<label for="status">Status Pesanan</label>
    						<select name="status" id="status" class="form-control" required>
    							<?php foreach ($statuspesanan as $sp) : ?>
    							<option value="<?= $sp['idstatus'] ?>"><?= $sp['statuspesanan'] ?></option>
    							<?php endforeach; ?>
    						</select>
    					</div>
    				</div>
    				<input type="hidden" name="idpembayaran" id="idpembayaran"
    					value="<?= $detail[0]['idpembayaran'] ?>">
    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    					<button type="submit" data-type="btn-update" data-id="" class="btn btn-primary">Update</button>
    				</div>
    			</form>
    		</div>
    	</div>
    </div>
    <!-- End of Main Content -->
    <script>
    	$('[data-type=detail]').click(function () {
    		var idpembayaran = $(this).attr('data-id');

    		location.href = '<?= base_url('
    		pembayaran / detail / ') ?>' + idpembayaran;
    	});

    	$('[data-type=update]').click(function () {
    		var modal = $('#exampleModalCenter');
    		var id = $(this).attr('data-id');
    		modal.find('[data-type=btn-update]').attr('data-id', id);
    		modal.modal();
    	});

    </script>
