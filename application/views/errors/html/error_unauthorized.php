<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title><?= $title ?></title>
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css') ?>">
</head>

<body>
	<div class="container">
		<div class="row mt-5">
			<div class="col-12 d-flex justify-content-center">
				<img src="<?= base_url('assets/img/illustration/user_group.png') ?>" alt="" width="500px" height="300px"
					class="img-fluid">
			</div>
			<div class="col-12 d-flex justify-content-center">
				<h3 class="text-secondary"><b>Unauthorized Group Policy</b></h3>
			</div>
			<div class="col-lg-12 d-flex justify-content-center">
				<p class="text-secondary">Kamu tidak dapat mengakses halaman ini, silahkan kembali ke halaman awal.</p>
			</div>
			<div class="col-lg-12 d-flex justify-content-center">
				<a href="<?= base_url('beranda') ?>" class="btn btn-success">Kembali ke halaman utama</a>
			</div>
		</div>
	</div>
</body>

</html>
