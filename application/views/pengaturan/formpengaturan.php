<!-- Main Content -->
<div id="content">
	<!-- Begin Page Content -->
	<div class="container-fluid">

		<!-- Page Heading -->
		<h1 class="h3 mb-4 text-gray-800"><?= $profile; ?></h1>
		<div class="card shadow mb-4">
			<div class="card-body">
				<form action="<?= base_url('pengaturan/formPengaturan') ?>" method="post" id="modal_post">
					<?php
                    if ($setting != null) {
                        $attr = 'readonly';
                        $val = 'edit';
                    } else {
                        $attr = '';
                        $val = 'simpan';
                    }
                    ?>
					<div class="form-group">
						<input type="text" class="form-control" id="idpengaturan" name="idpengaturan"
							placeholder="Kode Pengaturan" value="<?= ($setting == null) ? '' : $setting['kode'];  ?>"
							<?= $attr; ?>>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="namapengaturan" name="namapengaturan"
							placeholder="Nama Pengaturan" value="<?= ($setting == null) ? '' : $setting['nama'];  ?>">
					</div>
					<?php if($setting['kode'] == 'payment_mode'): ?>
					<div class="form-group">
						<select name="val" id="isi" class="form-control">
							<option value="development"
								<?= $setting == null ? '' : ($setting['isi'] == 'development' ? 'selected' : '') ?>>
								Development</option>
							<option value="production"
								<?= $setting == null ? '' : ($setting['isi'] == 'production' ? 'selected' : '') ?>>
								Production</option>
						</select>
					</div>
					<?php else:?>
					<div class="form-group">
						<textarea name="val" id="area" data-id="<?= ($setting == null) ? '' : $setting['kode']; ?>"
							rows="10" class="form-control"
							placeholder="Value"><?= ($setting == null) ? '' : $setting['isi'];  ?></textarea>
					</div>
					<?php endif;?>
					<div class="form-group">
						<button type="button" data-type="back" class="btn btn-secondary"
							data-dismiss="modal">Kembali</button>
						<button type="submit" class="btn btn-success">Simpan</button>
					</div>
					<input type="hidden" name="act" id="act" value="<?= $val; ?>">
					<input type="hidden" name="key" id="key">
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	var idpengaturan = $('#area').attr('data-id');

	$('[data-type=back]').click(function () {
		location.href = '<?= site_url('
		pengaturan ') ?>';
	});

	if (idpengaturan == 'pembayaran') {
		tinymce.init({
			selector: 'textarea',
			theme: 'modern',
			plugins: [
				'advlist autolink lists link image charmap print preview hr anchor pagebreak',
				'searchreplace wordcount visualblocks visualchars code fullscreen',
				'insertdatetime media nonbreaking save table contextmenu directionality',
				'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
			],
			toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
			toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
			image_advtab: true,
			templates: [{
					title: 'Test template 1',
					content: 'Test 1'
				},
				{
					title: 'Test template 2',
					content: 'Test 2'
				}
			],
			content_css: [
				'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
				'//www.tinymce.com/css/codepen.min.css'
			]
		});
	}

</script>
