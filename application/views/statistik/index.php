<div class="content">
    <div class="container-fluid">
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ml-3 mt-3 mr-3">
                            <?= $this->session->flashdata('message') ?>
                        </div>
                        <form action="<?= base_url('statistik') ?>" method="POST">
                            <div class="row mt-3 ml-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="from">Dari tanggal</label>
                                        <input type="date" name="from" id="from" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="to">Sampai tanggal</label>
                                        <input type="date" name="to" id="to" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="repp_type">Tipe Laporan</label>
                                        <select name="repp_type" id="repp_type" class="form-control">
                                            <option value="income">Pendapatan</option>
                                            <option value="sales">Penjualan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="type">Tipe Grafik</label>
                                        <select name="type" id="type" class="form-control">
                                            <option value="bar">Bar</option>
                                            <option value="line">Line</option>
                                            <option value="radar">Radar</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ml-4">
                                <button type="submit" class="btn btn-sm btn-primary">Tampilkan</button>
                            </div>
                        </form>
                        <div class="col-md-8">
                            <?php if (!empty($a_data)) : ?>
                                <?php if (!empty($statistik)) : ?>
                                    <table class="table">
                                        <tr>
                                            <td><b>Dari Tanggal</b> : <?= $a_data['from'] ?></td>
                                            <td><b>Sampai Tanggal</b> : <?= $a_data['to'] ?></td>
                                            <td><b>Tipe Laporan</b> : <?= $a_data['repp_type'] ?></td>
                                            <td><b>Tipe Grafik</b> : <?= $a_data['type'] ?></td>
                                        </tr>
                                    </table>
                                    <canvas id="myChart" width="200" height="200"></canvas>
                                <?php else : ?>
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <strong>Data tidak ditemukan!</strong> Coba inputkan tanggal dengan rentang yang berbeda.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: '<?= $tipe_bar ?>',
        data: {
            labels: [
                <?php
                $a_name = $a_type == "income" ? "tgl" : "namaproduk";
                if ($a_type == "sales") {
                    foreach ($statistik as $v) {
                        echo '"' . $v[$a_name] . '",';
                    }
                } else {
                    foreach ($statistik as $v) {
                        echo '"' . date('dMY', strtotime($v[$a_name])) . '",';
                    }
                }

                ?>
            ],
            datasets: [{
                label: '<?= $a_type == "sales" ? '# Terjual' : '# Pendapatan' ?>',
                data: [
                    <?php

                    if ($a_type == "sales") {
                        foreach ($statistik as $v) {
                            echo '"' . $v['terjual'] . '",';
                        }
                    } else {
                        foreach ($statistik as $v) {
                            echo '"' . ($v['jmlpayment'] + $v['jmlongkir']) . '",';
                        }
                    } ?>
                ],
                backgroundColor: [
                    <?php foreach ($statistik as $v) {
                        echo '"rgba(255, 99, 132, 0.2)",';
                    } ?>
                ],
                borderColor: [
                    <?php foreach ($statistik as $v) {
                        echo '"rgba(255, 99, 132, 1)",';
                    } ?>
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>