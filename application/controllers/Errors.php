<?php

class Errors extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function unauthorized()
    {
        $data['title'] = 'Error Unauthorized';

        $this->load->view('errors/html/error_unauthorized', $data);
    }


}
