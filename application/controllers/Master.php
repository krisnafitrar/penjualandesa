<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('token')) {
            redirect('auth');
        }

        isLoggedIn($this->session->userdata('token'), function($result){
            if(!$result){
                return redirect('auth');
            }
        }, true);

        $this->load->model('M_produk', 'produk');
        $this->load->model('M_jenis', 'jenis');
        $this->load->model('M_kategori', 'kategori');
        $this->load->model('M_desa', 'desa');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Master', site_url('home'));

        $data['title'] = 'Data Produk';
        $data['menu_now'] = 'Master';
        $data['user'] = $this->user;
        $data['produk'] = $this->produk->join(['kategori', 'jenisproduk', 'desa'], ['idkategori', 'idjenisproduk', 'iddesa'])->result_array();
        $this->template->load('template', 'master/index', $data);
    }

    public function jenis()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Master', site_url('master'));
        $this->breadcrumb->append_crumb('Jenis Produk', site_url('master/jenis'));

        $data['title'] = 'Data Jenis Produk';
        $data['menu_now'] = 'Master';
        $data['user'] = $this->user;
        $data['jenis'] = $this->jenis->get()->result_array();
        $this->template->load('template', 'master/jenis', $data);
    }

    public function kategori()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Master', site_url('master'));
        $this->breadcrumb->append_crumb('Kategori Produk', site_url('master/kategori'));

        $data['title'] = 'Data Kategori Produk';
        $data['menu_now'] = 'Master';
        $data['user'] = $this->user;
        $data['kategori'] = $this->kategori->get()->result_array();
        $this->template->load('template', 'master/kategori', $data);
    }

    public function desa()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Master', site_url('master'));
        $this->breadcrumb->append_crumb('Data Desa', site_url('master/desa'));

        $data['title'] = 'Data Desa';
        $data['menu_now'] = 'Master';
        $data['user'] = $this->user;
        $data['desa'] = $this->desa->get()->result_array();
        $this->template->load('template', 'master/desa', $data);
    }
}
