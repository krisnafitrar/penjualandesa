<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('token')) {
            redirect('auth');
        }

        isLoggedIn($this->session->userdata('token'), function($result){
            if(!$result){
                return redirect('auth');
            }
        }, true);


        $this->load->model('M_pembayaran', 'pembayaran');
        $this->load->model('M_pembayarandetail', 'pembayarandetail');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Transaksi', site_url('pembayaran'));

        $data['title'] = 'Data Transaksi';
        $data['menu_now'] = 'Transaksi';
        $data['user'] = $this->user;
        $data['pembayaran'] = $this->pembayaran->getPembayaran()->result_array();
        $this->template->load('template', 'pembayaran/index', $data);
    }

    public function detail($idpembayaran)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Transaksi', site_url('pembayaran'));
        $this->breadcrumb->append_crumb('Detail Transaksi', site_url('pembayaran/detail/') . $idpembayaran);

        $data['title'] = 'Detail Transaksi';
        $data['menu_now'] = 'Transaksi';
        $data['user'] = $this->user;
        $data['statuspesanan'] = $this->db->get('statuspesanan')->result_array();
        $data['detail'] = $this->pembayarandetail->getDetailPembayaran($idpembayaran)->result_array();
        $this->template->load('template', 'pembayaran/detail', $data);

        if ($_POST) {
            $id = $this->input->post('idpembayaran');
            $status = $this->input->post('status');

            $rec = [
                'idpembayaran' => $id,
                'idstatus' => $status
            ];

            $ins = $this->db->insert('tracking', $rec);
            $up = $this->db->update('pembayaran', ['statuspesanan' => $status], ['idpembayaran' => $id]);

            $ins && $up ? setMessage('Berhasil mengupdate status pesanan!', 'success') : setMessage('Gagal mengupdate pesanan, coba lagi!', 'danger');

            redirect('pembayaran/detail/' . $id);
        }
    }

    public function updateStatusPembayaran($status, $id)
    {
        $update = $this->pembayaran->update(['status' => $status], $id);
        $update ? setMessage('Berhasil mengubah status pembayaran', 'success') : setMessage('Gagal merubah status pembayaran', 'danger');
        redirect('pembayaran');
    }
}
