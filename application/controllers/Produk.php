<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_produk', 'produk');
        $this->load->model('M_users', 'users');
    }

    public function index()
    {
        $data['title'] = 'Produk';
        $keyword = $_GET['keyword'];
        $kategori = $_GET['kategori'];
        $sort = $_GET['sort'];

        if (!empty($keyword)) {
            $data['keyword'] = $keyword;
            $this->session->set_userdata('keyword', $data['keyword']);
        } else {
            if (isset($keyword)) {
                redirect('produk');
            }
        }

        //config pagination sesuai searchnya
        $this->db->like('namaproduk', $data['keyword']);
        $this->db->from('produk');
        $data['total_rows'] = $this->db->count_all_results();

        $limit = 200;
        setPagination($limit, $data['total_rows'], 'produk');
        $data['start'] = $this->uri->segment(3);
        $data['produk'] = $this->produk->getProdukLimit($limit, $data['start'], $data['keyword'], $kategori, $sort);
        $data['kategori'] = $this->db->get('kategori')->result_array();
        $data['active'] = 'produk';
        
        $this->template->load('client/templatefront', 'client/produk/index', $data);
    }

    public function detail($slug)
    {
        $data['title'] = 'Detail Produk';
        $data['active'] = 'produk';
        $data['produk'] = $this->produk->getDetailProduk($slug)->row_array();

        if(!$this->session->userdata('previous_url')){
            $this->session->set_userdata('previous_url', $_SERVER['PATH_INFO']);
        }

        if (empty($data['produk']))
            redirect('produk');

        $this->template->load('client/templatefront', 'client/produk/detail', $data);
    }

    public function wishlist()
    {
        $this->load->model('M_favorit', 'favorit');

        $act = $this->input->post('act');
        $idpembeli = $this->input->post('idpembeli');
        $idproduk = $this->input->post('idproduk');

        $data = [
            'idpembeli' => $idpembeli,
            'idproduk' => $idproduk
        ];

        switch ($act) {
            case 'add':
                $this->db->insert('favorit', $data);
                break;
            case 'del':
                $del = $this->favorit->deleteBy(['idpembeli' => $idpembeli, 'idproduk' => $idproduk]);
                break;
        }
    }
}
