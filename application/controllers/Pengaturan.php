<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengaturan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('token')) {
            redirect('auth');
        }

        isLoggedIn($this->session->userdata('token'), function($result){
            if(!$result){
                return redirect('auth');
            }
        }, true);

        $this->load->model('M_users');
        $this->load->model('M_setting', 'pengaturan');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Pengaturan', site_url('home'));

        $data['title'] = 'Pengaturan Aplikasi';
        $data['menu_now'] = 'Pengaturan';
        $data['user'] = $this->user;
        $data['pengaturan'] = $this->pengaturan->get()->result_array();
        $this->template->load('template', 'pengaturan/index', $data);
    }

    public function formPengaturan($id = 'tambah')
    {
        if ($id == 'tambah') {
            $title = 'Tambah Pengaturan';
            $key = '';
            $data['setting'] = $key;
        } else {
            $title = 'Edit Pengaturan';
            $data['pengaturan'] = $this->pengaturan->getBy(['idsetting' => $id])->row_array();
            $data['setting'] = $data['pengaturan'];
        }

        $data['title'] = $title;
        $data['profile'] = $title;
        $data['menu_now'] = 'Pengaturan';

        $data['user'] = $this->user;
        $this->template->load('template', 'pengaturan/formpengaturan', $data);

        if (!empty($_POST)) {
            $act = $this->input->post('act');
            $idpengaturan = $this->input->post('idpengaturan');
            $namapengaturan = $this->input->post('namapengaturan');
            if ($idpengaturan == 'pembayaran')
                $val = $this->input->post('val');
            else
                $val = $_POST['val'];

            $data = [
                'kode' => $idpengaturan,
                'nama' => $namapengaturan,
                'isi' => $val
            ];

            switch ($act) {
                case 'simpan':
                    $insert = $this->pengaturan->insert($data);
                    $msg = 'menambahkan pengaturan';
                    $insert ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    break;
                case 'edit':
                    $setting = $this->pengaturan->getBy(['kode' => $idpengaturan])->row_array();
                    $update = $this->pengaturan->update($data, $setting['idsetting']);
                    $msg = 'merubah pengaturan';
                    $update ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    break;
            }
            redirect('pengaturan');
        }
    }

    public function deletePengaturan($id)
    {
        $del = $this->pengaturan->delete($id);
        $del ? setMessage('Berhasil menghapus data!', 'success') : setMessage('Gagal menghapus data!', 'danger');
        redirect('pengaturan');
    }
}
