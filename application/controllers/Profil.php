<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends MY_Controller
{
    public $user;

    function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('token')) {
            redirect('auth');
        }

        isLoggedIn($this->session->userdata('token'), function($result){
            if(!$result){
                return redirect('auth');
            }
        });

        if (empty(getPembeli())) {
            redirect('beranda');
        }

        $this->load->model('M_users', 'users');
        $this->load->model('M_pembeli', 'pembeli');
        $this->load->model('M_provinsi', 'provinsi');
        $this->load->model('M_kota', 'kota');
        $this->load->model('M_kecamatan', 'kecamatan');
        $this->user = $this->users->getBy(['email' => $this->session->userdata('email')])->row_array();
    }

    public function index()
    {
        $data['title'] = "Profil";
        $data['active'] = "profil";
        $data['users'] = $this->user;
        $pembeli = getPembeli()->row_array();

        $percent = 100;
        $count = 0;

        foreach ($pembeli as $val) {
            if($val != null){
                $count += 1;
            }
        }

        $percent = ($count / count($pembeli)) * $percent;

        $data['persentase_profil'] = $percent;

        $this->template->load('client/templateprofil', 'client/profil/index', $data);
    }

    public function edit()
    {
        $data['title'] = "Profil";
        $data['active'] = "edit";

        $data['users'] = $this->user;
        $data['pembeli'] = getPembeli()->row_array();
        $data['jk'] = [
            'L' => 'Laki-Laki',
            'P' => 'Perempuan'
        ];

        $this->template->load('client/templateprofil', 'client/profil/edit', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $tanggal_lahir = $this->input->post('tanggal_lahir');
            $telepon = $this->input->post('telepon');
            $jk = $this->input->post('jk');
            $idpembeli = $this->input->post('idpembeli');


            $pembeli = [
                'namapembeli' => $nama,
                'telepon' => $telepon,
                'tanggal_lahir' => $tanggal_lahir,
                'jk' => $jk
            ];

            $this->pembeli->beginTrans();
            $this->pembeli->update($pembeli, $idpembeli);
            $this->users->update(['nama' => $nama], $this->user['iduser']);
            $status = $this->pembeli->statusTrans();
            $this->pembeli->commitTrans($status);

            $status && $status ? setMessage('Berhasil merubah profil', 'success') : setMessage('Gagal mengubah profil', 'danger');
            redirect('profil/edit');
        }
    }

    public function password()
    {
        $data['title'] = "Profil";
        $data['active'] = "password";

        $this->template->load('client/templateprofil', 'client/profil/password', $data);

        if ($_POST) {
            $oldpass = $this->input->post('oldpassword');
            $newpass = $this->input->post('newpassword');
            $newpassconf = $this->input->post('newpasswordconf');

            if (password_verify($oldpass, $this->user['password'])) {
                if ($newpass == $newpassconf) {
                    if ($newpass != $oldpass) {
                        $update = $this->users->update(['password' => password_hash($newpass, PASSWORD_DEFAULT)], $this->user['iduser']);
                        $update ? setMessage('Berhasil merubah password', 'success') : setMessage('Gagal merubah password!', 'danger');
                    } else {
                        setMessage('Password tidak boleh sama dengan yang lama!', 'danger');
                    }
                } else {
                    setMessage('Password tidak cocok!', 'danger');
                }
            } else {
                setMessage('Password salah!', 'danger');
            }

            redirect('profil/password');
        }
    }

    public function alamat($act = null, $id = null)
    {
        $this->load->model('M_alamat', 'alamat');

        $data['title'] = "Profil";
        $data['active'] = "alamat";

        if (empty($act)) {
            $data['alamat'] = $this->alamat->getList(['idpembeli' => $this->user['idpembeli']]);

            $a_kolom = [];
            $a_kolom[] = ['kolom' => 'no', 'label' => 'No'];
            $a_kolom[] = ['kolom' => 'jenisalamat', 'label' => 'Nama Alamat'];
            $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Detail Alamat'];

            $data['a_kolom'] = $a_kolom;
        }


        if (!empty($act)) {
            $a_kolom = [];
            $a_kolom[] = ['kolom' => 'jenisalamat', 'label' => 'Nama Alamat', 'required' => true];
            $a_kolom[] = ['kolom' => 'penerima', 'label' => 'Nama Penerima', 'required' => true];
            $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Alamat', 'required' => true];
            $a_kolom[] = ['kolom' => 'idprovinsi', 'label' => 'Provinsi', 'type' => 'S2', 'required' => true, 'default' => 'enabled', 'real_col' => 'province_name'];
            $a_kolom[] = ['kolom' => 'idkota', 'label' => 'Kota/Kabupaten','type' => 'S2', 'required' => true, 'default' => 'disabled', 'real_col' => 'city_name'];
            $a_kolom[] = ['kolom' => 'idkecamatan', 'label' => 'kecamatan','type' => 'S2', 'required' => true, 'default' => 'disabled', 'real_col' => 'subdistrict_name'];
            $a_kolom[] = ['kolom' => 'kodepos', 'label' => 'Kode Pos', 'required' => true];

            if ($_POST['act'] == 'save') {
                $record = [];
                foreach ($a_kolom as $col) {
                    if (!empty($_POST[$col['kolom']]) || $col['required'] == true) {
                        if (empty($_POST[$col['kolom']])) {
                            setMessage($col['label'] . " wajib diisi.", 'danger');
                            if (empty($_POST['key']))
                                redirect('profil/alamat/add');
                            else
                                redirect('profil/alamat/detail/' . $_POST['key']);
                        }
                        $record[$col['kolom']] = $_POST[$col['kolom']];
                    }
                }

                $record['idpembeli'] = $this->user['idpembeli'];

                if (empty($_POST['key'])) {
                    $ok = $this->alamat->insert($record);
                    $msg = "menambah alamat";
                    $ok ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    if (!$ok)
                        redirect('profil/alamat/add');
                } else {
                    $ok = $this->alamat->update($record, $_POST['key']);
                    $msg = "mengubah alamat";
                    $ok ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    if (!$ok)
                        redirect('profil/alamat/detail/' . $_POST['key']);
                }
                redirect('profil/alamat');
            }

            $data['a_kolom'] = $a_kolom;

            switch ($act) {
                case 'add':
                    $this->template->load('client/templateprofil', 'client/profil/act_alamat', $data);
                    break;
                case 'detail':
                    $data['row'] = $this->alamat->joinAll($id)->row_array();
                    if (empty($data['row']))
                        redirect('profil/alamat');

                    $this->template->load('client/templateprofil', 'client/profil/act_alamat', $data);
                    break;
                default:
                    redirect('profil/alamat');
                    break;
            }
        } else {
            $this->template->load('client/templateprofil', 'client/profil/alamat', $data);
        }
    }

    public function getProvince()
    {
        $keyword = $this->input->post('cari');
        $query = "lower(province_name) like '%" . strtolower($keyword) . "%'";
        $provinsi = $this->provinsi->getBy($query);

        if ($provinsi->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($provinsi->result_array() as $val) {
                $data[] = array('id' => $val['province_id'], 'text' => $val['province_name']);
            }
            echo json_encode($data);
        }
    }

    public function getCity()
    {
        $keyword = $this->input->post('cari');
        $idprovinsi = $this->input->post('idprovinsi');
        $query = "lower(city_name) like '%" . strtolower($keyword) . "%' and province_id=$idprovinsi";
        $kota = $this->kota->getBy($query);

        if ($kota->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($kota->result_array() as $val) {
                $data[] = array('id' => $val['city_id'], 'text' => $val['city_name']);
            }
            echo json_encode($data);
        }
    }

    public function getSubdistrict()
    {
        $keyword = $this->input->post('cari');
        $idkota = $this->input->post('idkota');
        $query = "lower(subdistrict_name) like '%" . strtolower($keyword) . "%' and city_id=$idkota";
        $kota = $this->kecamatan->getBy($query);

        if ($kota->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($kota->result_array() as $val) {
                $data[] = array('id' => $val['subdistrict_id'], 'text' => $val['subdistrict_name']);
            }
            echo json_encode($data);
        }
    }

}
