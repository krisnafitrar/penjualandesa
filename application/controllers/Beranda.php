<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_produk', 'produk');
        $this->load->model('M_users', 'users');

        $this->merchant_id = config_item('merchant_id');
        $this->client_key = config_item('client_key');
        $this->server_key = config_item('server_key');

    }

    public function index()
    {
        $data['title'] = 'Beranda';
        $data['active'] = 'beranda';
        $data['produk'] = $this->produk->getProduk()->result_array();
        $data['pembeli'] = $this->users->join(['pembeli'], ['idpembeli'], null, ['email' => $this->session->userdata('email')])->row_array();

        $this->template->load('client/templatefront', 'client/home/index', $data);
    }

    public function wishlist()
    {
        $pembeli = getPembeli()->row_array();

        if (!$this->session->userdata('email')) {
            setMessage('Harap login terlebih dahulu', 'danger');
            redirect('auth');
        }

        if (empty($pembeli)) {
            setMessage('Hanya pembeli yang dapat mengakses halaman ini', 'danger');
            redirect('beranda');
        }

        $this->load->model('M_favorit', 'favorit');
        $this->load->model('M_pembeli', 'pembeli');


        $data['title'] = 'Wishlist';
        $data['wishlist'] = $this->favorit->favoritPembeli($pembeli['idpembeli'])->result_array();

        $this->template->load('client/templatefront', 'client/home/wishlist', $data);
    }
}
