<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment extends MY_Controller
{
    private $merchant_id;
    private $client_key;
    private $server_key;
    private $pembeli;

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('token')) {
            redirect('auth');
        }

        isLoggedIn($this->session->userdata('token'), function($result){
            if(!$result){
                return redirect('auth');
            }
        });

        $this->pembeli = getPembeli()->row_array();
        $this->merchant_id = settingSIM()['merchant_id'];
        $this->client_key = settingSIM()['client_key'];
        $this->server_key = settingSIM()['server_key'];

    }

    public function pay($idpembayaran)
    {
        $data['title'] = 'Payment';
        $data['merchantid'] = $this->merchant_id;
        $data['client_key'] = $this->client_key;
        $data['server_key'] = $this->server_key;
        $data['paysuccess'] = false;

        $record = $this->db->get_where('pembayaran', ['idpembayaran' => $idpembayaran])->row_array();

        if(!empty($record['payment_token'])){
            redirect('transaksi/pesanansaya');
        }

        $data['token'] = $this->apipayment($record, function($response) use ($record){
            if($response['status']){
                //update token db
                $ok = $this->db->update('pembayaran', ['payment_token' => $response['token'], 'payment_payloads' => json_encode($response)], ['invoice' => $record['invoice']]);

                if(!$ok){
                    throw new Exception('Failed to update pembayaran!', 501);
                }

                return $response['token'];
            }
        });

        $this->template->load('client/templatefront', 'client/payment/index', $data);
    }

    public function paymentsuccess()
    {
        $data['title'] = 'Payment';
        $data['paysuccess'] = true;
        $order_invoice = $_GET['order_id'];

        $this->db->update('pembayaran', ['status_pembayaran' => 1, 'statuspesanan' => 1], ['invoice' => $order_invoice]);

        $this->template->load('client/templatefront', 'client/payment/index', $data);
    }

    private function apipayment($record,$callback)
    {
        $user_name = explode(" ",$this->user['nama']);
        $firstname = $user_name[0];
        $lastname = end($user_name);

        $type = settingSIM()['payment_mode'];

        switch ($type) {
            case 'development':
                $url = settingSIM()['payment_development_url'];
                break;
            case 'production':
                $url = settingSIM()['payment_production_url'];    
                break;
            default:
                throw new Exception("Error Processing Request", 501);
                break;
        }

        $server_key = $this->server_key;

        $request = [
            'transaction_details' => ['order_id' => $record['invoice'], 'gross_amount' => $record['grand_total']],
            'credit_card' => ['secure' => true],
            'customer_details' => ['first_name' => $firstname, 'last_name' => $lastname, 'email' => $this->user['email'], 'phone' => $this->pembeli['telepon']]
        ];

        $reqjson = json_encode($request);

        $headers = array('Content-Type: application/json', 'Accept: application/json');
        if (empty($token)) {
            $authstring = base64_encode($server_key . ':');
            $headers[] = 'Authorization: Basic ' . $authstring;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $reqjson);
        $responsejson = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $response = json_decode($responsejson, true);
        if ($info['http_code'] == 201) {
            $res = ['status' => true] + $response;
        } else {
            $res = ['status' => false];
        }

        return $callback($res);
    }

    
}
