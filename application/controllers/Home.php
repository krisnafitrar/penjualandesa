<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        
        if (!$this->session->userdata('token')) {
            redirect('auth');
        }

        isLoggedIn($this->session->userdata('token'), function($result){
            if(!$result){
                return redirect('auth');
            }
        }, true);

        //set default
        $this->title = 'Beranda';
        $this->menu = 'home';
        $this->icon = 'home';
    }
}
