<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Auth extends MY_Controller
{
    private $max_attempts = 7;
    private $waiting_time_in_minute = 2;
    private $token_valid_in_hours;

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_users', 'users');
        $this->load->model('M_pembeli', 'pembeli');

        $this->token_valid_in_hours = config_item('sess_expiration') / 3600;
    }

    public function index()
    {
        if ($this->session->userdata('email')) {
            redirect('home');
        }

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        
        if ($this->form_validation->run() == false) {
            $data['title'] = "Login Page";
            $this->template->load('client/templatefront', 'auth/index', $data);
        } else {
            $this->doLogin();
        }
    }

    public function registrasi()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('notelp', 'Nomor Handphone', 'required|trim');
        $this->form_validation->set_rules('password', 'Kata Sandi', 'required|trim|min_length[8]');
        $this->form_validation->set_rules('repeatpassword', 'Konfirmasi Kata Sandi', 'required|trim|matches[password]');

        if($this->form_validation->run() == false){
            $data['title'] = "Registrasi";
            $this->template->load('client/templatefront', 'auth/registrasi', $data);
        }else{
            $this->doRegister();
        }
    }

    private function doLogin()
    {
        if ($this->session->userdata('email')) {
            redirect('user');
        }

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = getUserDetails($email);

        $ip_address = $_SERVER['REMOTE_ADDR'];
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $login_data = [
            'email' => $email,
            'ip_address' => $ip_address,
            'user_agent' => $user_agent,
        ];


        $login_sess = $this->session->userdata($ip_address . '_failed') ? $this->session->userdata($ip_address . '_failed') : 0;
        $latest_attempt = $this->session->userdata($ip_address . 'latest_attempt');
        $waiting_time_login = $this->session->userdata($ip_address . '_waiting_time');


        if(!empty($waiting_time_login)){
            if(time() < $waiting_time_login){
                $time = $waiting_time_login - time();
    
                if($time > 60){
                    $minute = floor($time / 60);
                    $second = $time % 60;
                }

                setMessage('Anda sudah melakukan percobaan masuk lebih dari ' . $this->max_attempts . ' kali, mohon tunggu selama ' . ($minute ? $minute . ' menit ' . $second . ' detik' : $time . ' detik'), 'danger');
                redirect('auth');            
            }else{
                session_unset($ip_address . '_failed');
                session_unset($ip_address . '_waiting_time');
            }
        }

        //jika usernya ada
        if ($user) {
            //cek password
            if (password_verify($password, $user['password'])) {
                $token = random_str(32);

                $login_data['flag'] = 1;
                $login_data['token'] = $token;
                $login_data['valid_until'] = date('Y-m-d H:i:s', time() + (60 * 60 * $this->token_valid_in_hours));

                $this->db->insert('login_attempts', $login_data);
                

                //siapkan session
                $data = [
                    'idrole' => $user['idrole'],
                    'email' => $user['email'],
                    'token' => $token
                ];

                $this->session->set_userdata($data);

                if ($this->session->userdata['idrole'] == "") {
                    if($url = $this->session->userdata('previous_url')){
                        redirect($url);
                    }else{
                        redirect('beranda');
                    }
                    
                } else {
                    redirect('home');
                }

            } else {
                //insert login attempt
                $login_data['flag'] = 0;

                $this->db->insert('login_attempts', $login_data);

                if($login_sess >= $this->max_attempts){
                    $this->session->set_userdata($ip_address . '_waiting_time', time() + 60 * $this->waiting_time_in_minute);                    
                }else{
                    if(!$latest_attempt or (time() - $latest_attempt <= 60)){
                        $this->session->set_userdata($ip_address . '_failed', $login_sess + 1);
                        $this->session->set_userdata($ip_address . '_latest_attempt', time());
                    }else{
                        session_unset($ip_address . '_failed');
                    }
                }
                
                //password salah
                setMessage('Password salah !', 'danger');
                redirect('auth');
            }
        } else {
            if($login_sess >= $this->max_attempts){
                $this->session->set_userdata($ip_address . '_waiting_time', time() + 60 * $this->waiting_time_in_minute);                    
            }else{
                if(!$latest_attempt or (time() - $latest_attempt <= 60)){
                    $this->session->set_userdata($ip_address . '_failed', $login_sess + 1);
                    $this->session->set_userdata($ip_address . '_latest_attempt', time());
                }else{
                    session_unset($ip_address . '_failed');
                }
            }
            //usernya tidak ada
            setMessage('User tidak terdaftar', 'danger');
            redirect('auth');
        }
    }

    private function doRegister()
    {
        if (!empty($_POST)) {
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $telp = $this->input->post('notelp');
            $password1 = $this->input->post('password');

            
            $idpembeli = bin2hex(random_bytes(8));

            $payload = [
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                'remote_port' => $_SERVER['REMOTE_PORT'],
                'gateway_interface' => $_SERVER['GATEWAY_INTERFACE'],
                'request_time_float' => $_SERVER['REQUEST_TIME_FLOAT'],
                'request_time' => $_SERVER['REQUEST_TIME'],
                'redirect_status' => $_SERVER['REDIRECT_STATUS']
            ];

            $data = [
                'iduser' => bin2hex(random_bytes(8)),
                'nama' => htmlspecialchars($nama),
                'email' => htmlspecialchars($email),
                'password' => password_hash($password1, PASSWORD_DEFAULT),
                'idpembeli' => $idpembeli,
                'is_loggedin' => 0,
                'payload' => json_encode($payload),
                'is_aktif' => 1
            ];

            $pembeli = [
                'idpembeli' => $idpembeli,
                'namapembeli' => $nama,
                'telepon' => $telp,
            ];

            if($this->db->get_where('users', ['email' => $email])->row_array()){
                setMessage('Email sudah terdaftar, gunakan email lainnya!', 'danger');
                redirect('auth/registrasi');
            }

            $this->pembeli->beginTrans();
            $this->pembeli->insert($pembeli);
            $this->users->insert($data);

            $check = $this->pembeli->statusTrans();
            $this->pembeli->commitTrans($check);

            $check && $check ? setMessage('Berhasil membuat akun', 'success') : setMessage('Gagal membuat akun!', 'danger');

            redirect('auth/registrasi');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('idrole');
        $this->session->unset_userdata('token');
        $this->session->unset_userdata('keyword');
        $this->session->unset_userdata('previous_url');
        redirect('beranda');
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }
}
