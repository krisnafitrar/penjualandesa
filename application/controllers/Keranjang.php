<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keranjang extends MY_Controller
{
    private $pembeli;
    private $originCity;

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('token')) {
            redirect('beranda');
        }

        isLoggedIn($this->session->userdata('token'), function ($result) {
            if (!$result) {
                unset_list(array('nama', 'username', 'email', 'token'));
                return redirect('beranda');
            }
        });

        $this->pembeli = getPembeli()->row_array();
        $this->originCity = 501; //asumsikan kota asal adalah jogja


        $this->load->model('M_keranjang', 'keranjang');
        $this->load->model('M_users', 'users');
    }

    public function index()
    {
        $this->load->model('M_alamat', 'alamat');
        $data['title'] = 'Keranjang';
        $data['keranjang'] = getCart()->result_array();
        $data['alamat'] = $this->alamat->joinById($this->pembeli['idpembeli'])->result_array();
        $data['origin_city'] = $this->originCity;

        $this->template->load('client/templatefront', 'client/keranjang/index', $data);
    }

    public function checkoutberhasil()
    {
        // insert ke pembayaran dan pembayaran detail
        $this->load->model('M_pembayaran', 'pembayaran');
        $this->load->model('M_pembayarandetail', 'pembayarandetail');
        $this->load->model('M_produk', 'produk');
        $this->load->model('M_tracking', 'tracking');

        $data['title'] = 'Pembelian';
        $data['settingSIM'] = settingSIM();
        $pembeli = getPembeli()->row_array();

        if ((empty($_POST['act']) || $_POST['act'] != 'checkout') && empty($this->session->userdata('idpembayaran'))) {
            setMessage("Anda tidak dapat mengakses halaman ini", 'danger');
            redirect('keranjang');
        } else if ($_POST['act'] == 'checkout') {
            $this->session->set_userdata('idpembayaran', null);

            // ambil keranjang
            $keranjang = $this->keranjang->getKeranjangDetail($pembeli['idpembeli'])->result_array();

            // cek stok
            foreach ($keranjang as $row) {
                if ($row['jumlah'] > $row['stok']) {
                    setMessage("Order Anda pada Produk " . $row['namaproduk'] . " melebihi stok yang tersedia", 'danger');
                    redirect('keranjang');
                }
            }

            $this->pembayaran->beginTrans();

            $record = [];
            $record['idalamat'] =  $_POST['alamat'];
            $record['idpembeli'] = $pembeli['idpembeli'];
            $record['status_pembayaran'] = 0;
            $record['invoice'] = 'INV' . time();
            $record['tgl'] = date('Y-m-d');
            $record['statuspesanan'] = 0;
            $record['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $record['discount_amount'] = 0;
            $record['origin_city'] = $this->originCity;
            $record['idcourier'] = $_POST['courier_id'];
            $record['courier_service'] = $_POST['courier_service'];
            $record['destination_city'] = $_POST['destination_city'];
            $record['delivery_cost'] = $_POST['ongkir'];
            $record['courier_payloads'] = $_POST['courier_payloads'];


            foreach ($keranjang as $row) {
                $record['total_amount'] += $row['jumlah'] * $row['harga'];
            }

            $record['grand_total'] = (intval($record['total_amount']) - intval($record['discount_amount'])) + intval($record['delivery_cost']);


            // insert pembayaran
            list($ok, $idpembayaran) = $this->pembayaran->insert($record, true);

            if ($ok) {
                $rec = [];
                $rec['idpembayaran'] = $idpembayaran;
                foreach ($keranjang as $row) {
                    $rec['idproduk'] = $row['idproduk'];
                    $rec['jumlah'] = $row['jumlah'];
                    $rec['harga'] = $row['harga'];

                    // insert pembayaran detail
                    $ok = $this->pembayarandetail->insert($rec);

                    if (!$ok){
                        throw new Exception("Error Processing Pembayaran Detail", 1);
                        break;
                    }
                    else {
                        $recp = [];
                        $recp['stok'] = $row['stok'] - $row['jumlah'];

                        // update stok produk
                        $ok = $this->produk->update($recp, $row['idproduk']);
                        if (!$ok){
                            throw new Exception("Error Update Stok", 1);
                            break;
                        }
                    }
                }
            }

            if ($ok) {
                $rect = [];
                $rect['idpembayaran'] = $idpembayaran;
                $rect['idstatus'] = '0';

                // insert tracking
                $ok = $this->tracking->insert($rect);

            }else{
                throw new Exception("Error Processing Update Stock", 1);
            }

            if ($ok) {
                $ok = $this->keranjang->deleteBy(['idpembeli' => $pembeli['idpembeli']]);
            }else{
                throw new Exception("Error Processing Tracking", 1);
            }

            $check = $this->pembayaran->statusTrans();

            $this->pembayaran->commitTrans($check);

            if (!$check) {
                setMessage("Proses checkout gagal, silakan coba kembali", 'danger');
                redirect('keranjang');
            } else {
                $this->session->set_userdata('idpembayaran', $idpembayaran);
                redirect('payment/pay/' . $idpembayaran);
            }
        }
    }

    public function cart()
    {
        $act = $this->input->post('act');
        $idpembeli = $this->input->post('idpembeli');
        $idproduk = $this->input->post('idproduk');
        $idkeranjang = $this->input->post('idkeranjang');
        $jumlah = $this->input->post('jumlah');

        $data = [
            'idpembeli' => $idpembeli,
            'idproduk' => $idproduk,
            'jumlah' => $jumlah
        ];

        //cek keranjang udah ada belum
        switch ($act) {
            case 'add':
                $cek = $this->db->get_where('keranjang', ['idpembeli' => $idpembeli, 'idproduk' => $idproduk]);

                if ($cek->num_rows() > 0) {
                    $update = $this->db->update('keranjang', ['jumlah' => $cek->row_array()['jumlah'] + $jumlah], ['idpembeli' => $idpembeli, 'idproduk' => $idproduk]);
                    $update ? setMessage('Berhasil menambahkan ke keranjang', 'success') : setMessage('Gagal menambahkan ke keranjang', 'danger');
                } else {
                    $insert = $this->keranjang->insert($data);
                    $insert ? setMessage('Berhasil menambahkan ke keranjang', 'success') : setMessage('Gagal menambahkan ke keranjang', 'danger');
                }
                break;
            case 'del':
                $del = $this->db->delete('keranjang', ['idpembeli' => $idpembeli, 'idproduk' => $idproduk]);
                break;
            case 'update':
                $update = $this->db->update('keranjang', ['jumlah' => $jumlah], ['idkeranjang' => $idkeranjang]);
                break;
        }
    }

    public function cekOngkir()
    {
        $key = "70d325bfd92d4e9eff8c1d3ef0e553ed";
        $origin = $this->input->post('origin');
        $destination = $this->input->post('destination');
        $weight = $this->input->post('weight');
        $courier = $this->input->post('courier');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "origin=" . $origin . "&destination=" . $destination . "&weight=" . $weight . "&courier=" . $courier . "",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: $key"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo 'Error : ' + $err;
        } else {
            $res = json_decode($response, true);
            $result = $res['rajaongkir']['results'];
            echo json_encode($result);
        }
    }
}
