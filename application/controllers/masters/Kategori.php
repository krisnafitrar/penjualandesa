<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Kategori Produk';
        $this->menu = 'kategori';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'idkategori', 'label' => 'ID'];
        $a_kolom[] = ['kolom' => 'namakategori', 'label' => 'Nama kategori'];

        $this->a_kolom = $a_kolom;
    }
}
