<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_kategori', 'kategori');
        $this->load->model('M_desa', 'desa');
        $this->load->model('M_jenis', 'jenis');

        //set default
        $this->title = 'Produk';
        $this->menu = 'produk';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kategori = $this->kategori->getListCombo();
        $a_desa = $this->desa->getListCombo();
        $a_jenis = $this->jenis->getListCombo();
        $a_status = [
            '0' => 'Habis',
            '1' => 'Tersedia'
        ];
        $a_isambil = [
            '1' => 'Ya',
            '2' => 'Tidak'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'idproduk', 'label' => 'ID Produk'];
        $a_kolom[] = ['kolom' => 'namaproduk', 'label' => 'Nama Produk'];
        $a_kolom[] = ['kolom' => 'iddesa', 'label' => 'Desa', 'type' => 'S', 'option' => $a_desa];
        $a_kolom[] = ['kolom' => 'stok', 'label' => 'Stok', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'status', 'label' => 'Status', 'type' => 'S', 'option' => $a_status];
        $a_kolom[] = ['kolom' => 'idkategori', 'name' => 'namakategori', 'label' => 'Kategori', 'type' => 'S', 'option' => $a_kategori];
        $a_kolom[] = ['kolom' => 'idjenisproduk', 'label' => 'Jenis', 'type' => 'S', 'option' => $a_jenis];
        $a_kolom[] = ['kolom' => 'harga', 'label' => 'Harga', 'set_currency' => true, 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'isambilsendiri', 'label' => 'Ambil sendiri', 'is_tampil' => false, 'type' => 'S', 'option' => $a_isambil];
        $a_kolom[] = ['kolom' => 'deskripsi', 'label' => 'Deskripsi', 'is_tampil' => false, 'type' => 'A'];
        $a_kolom[] = ['kolom' => 'foto', 'label' => 'Gambar', 'is_tampil' => false, 'type' => 'F', 'path' => './assets/img/produk/', 'file_type' => 'jpg|png'];

        $this->a_kolom = $a_kolom;
    }
}
