<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }
        $this->load->model('M_users','users');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Master', site_url('home'));
        $this->breadcrumb->append_crumb('Data User', site_url('user'));

        $data['title'] = 'Data User';
        $data['menu_now'] = 'Master';
        $data['user'] = $this->user;
        $data['datauser'] = $this->users->get()->result_array();
        $this->template->load('template', 'master/user', $data);

        if ($_POST) {
            $act = $this->input->post('act');
            $key = $this->input->post('key');
            $nama = $this->input->post('nama');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $role = $this->input->post('role');
            $status = $this->input->post('status');

            switch ($act) {
                case 'simpan':

                    break;

                default:
                    # code...
                    break;
            }
        }
    }

    public function getUser($id)
    {
        echo json_encode($this->user->getBy(['iduser' => $id])->row_array());
    }
}
