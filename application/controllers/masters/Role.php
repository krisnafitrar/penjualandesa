<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Role extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Role';
        $this->menu = 'role';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'idrole', 'label' => 'ID'];
        $a_kolom[] = ['kolom' => 'namarole', 'label' => 'Nama Role'];

        $this->a_kolom = $a_kolom;
    }
}
