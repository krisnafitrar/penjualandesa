<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Jenis Produk';
        $this->menu = 'jenis';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'idjenisproduk', 'label' => 'ID'];
        $a_kolom[] = ['kolom' => 'namajenisproduk', 'label' => 'Nama Jenis'];

        $this->a_kolom = $a_kolom;
    }
}
