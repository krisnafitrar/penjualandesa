<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends MY_Controller
{
    private $pembeli;

    function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('token')) {
            redirect('auth');
        }

        isLoggedIn($this->session->userdata('token'), function($result){
            if(!$result){
                return redirect('auth');
            }
        });

        $this->pembeli = getPembeli()->row_array();

        $this->load->model('M_pembayaran', 'pembayaran');
        $this->load->model('M_pembayarandetail', 'pembayarandetail');
        $this->load->model('M_tracking', 'tracking');
        $this->load->model('M_statuspesanan', 'statuspesanan');
    }

    public function pesanansaya()
    {
        $data['title'] = 'Pesanan Saya';
        $data['pesanan'] = $this->pembayaran->getList(['idpembeli' => $this->pembeli['idpembeli']]);
        $a_status = $this->statuspesanan->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'no', 'label' => 'No'];
        $a_kolom[] = ['kolom' => 'time', 'label' => 'Tgl Pemesanan', 'type' => 'D'];
        $a_kolom[] = ['kolom' => 'grand_total', 'label' => 'Jumlah Pembayaran', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'status_pembayaran', 'label' => 'Status Pembayaran', 'type' => 'S', 'option' => ['0' => 'Belum bayar', '1' => 'Sudah bayar']];
        $a_kolom[] = ['kolom' => 'statuspesanan', 'label' => 'Status Pesanan', 'type' => 'S', 'option' => $a_status];


        $data['a_kolom'] = $a_kolom;

        $this->template->load('client/templatefront', 'client/transaksi/pesanan', $data);
    }

    public function detailpesanan($idpembayaran)
    {
        $data['title'] = 'Detail Pesanan';
        $data['detail'] = $this->pembayarandetail->getDetailPembayaran($idpembayaran)->result_array();
        $this->template->load('client/templatefront', 'client/transaksi/detailpesanan', $data);
    }

    public function uploadBukti()
    {
        if ($_POST || $_FILES) {
            $idpembayaran = $this->input->post('idpembayaran');
            $oldbukti = $this->input->post('oldbukti');
            $foto = $_FILES['buktibayar'];
            $path = './assets/img/buktibayar';

            if ($foto) {
                $config['upload_path'] = $path;
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size'] = 2048;
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('buktibayar')) {
                    unlink(FCPATH . $path . $oldbukti);
                    $foto = $this->upload->data('file_name');
                } else {
                    setMessage($this->upload->display_errors(), 'danger');
                }
            } else {
                setMessage('Anda belum upload bukti!', 'danger');
            }

            $record = ['buktibayar' => $foto];
            $ok = $this->pembayaran->update($record, $idpembayaran);
            $ok ? setMessage('Berhasil mengupload bukti, tunggu konfirmasi dari kami', 'success') : setMessage('Gagal mengupload bukti!', 'danger');

            redirect('transaksi/detailpesanan/' . $idpembayaran);
        }
    }
}
