<?php

require FCPATH . "/vendor/autoload.php";

class Laporan extends MY_Controller
{
    private $PDF;

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('token')) {
            redirect('auth');
        }

        isLoggedIn($this->session->userdata('token'), function($result){
            if(!$result){
                return redirect('auth');
            }
        }, true);

        $this->load->model('M_users', 'users');
        $this->load->model('M_pembayaran', 'pembayaran');
        
        $this->PDF = new \Mpdf\Mpdf();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url('home'));
        $this->breadcrumb->append_crumb('Statistik', site_url('statistik'));

        if (!empty($_POST)) {
            $type = $this->input->post('type');
            $from = $this->input->post('from');
            $to = $this->input->post('to');

            if ($type == "income") {
                $a_tipe = "income";
                $a_data = $this->pembayaran->getLaporan($from, $to)->result_array();
            } else {
                $a_tipe = "sales";
                $a_data = $this->pembayaran->getStatistik($from, $to)->result_array();
            }

            $a_info = [
                'from' => $from,
                'to' => $to,
                'report_type' => $type == 'income' ? 'Pendapatan' : 'Penjualan'
            ];
        }

        $data['title'] = 'Laporan';
        $data['user'] = $this->user;
        $data['a_tipe'] = $a_tipe;
        $data['a_data'] = $a_data;
        $data['a_info'] = $a_info;
        $this->template->load('template', 'laporan/index', $data);
    }

    public function cetak()
    {
        if (!empty($_POST)) {
            $tipe_laporan = $this->input->post('tipe_laporan');
            $from = $this->input->post('from_date');
            $to = $this->input->post('to_date');

            if ($tipe_laporan == "Pendapatan") {
                $a_data = $this->pembayaran->getLaporan($from, $to)->result_array();
                $a_kolom = [];
                $a_kolom[] = ['kolom' => ':no', 'label' => 'No'];
                $a_kolom[] = ['kolom' => 'tgl', 'label' => 'Tanggal'];
                $a_kolom[] = ['kolom' => 'jmlpayment', 'label' => 'Total Pendapatan', 'is_currency' => true];
            } else {
                $a_data = $this->pembayaran->getStatistik($from, $to)->result_array();
                $a_kolom = [];
                $a_kolom[] = ['kolom' => ':no', 'label' => 'No'];
                $a_kolom[] = ['kolom' => 'tgl', 'label' => 'Tanggal'];
                $a_kolom[] = ['kolom' => 'idproduk', 'label' => 'Kode Produk'];
                $a_kolom[] = ['kolom' => 'namaproduk', 'label' => 'Nama Produk'];
                $a_kolom[] = ['kolom' => 'terjual', 'label' => 'Terjual'];
            }

            $a_info = [
                'from' => $from,
                'to' => $to,
                'report_type' => $tipe_laporan == 'Pendapatan' ? 'Pendapatan' : 'Penjualan'
            ];

            $data['repp_val'] = $a_data;
            $data['a_info'] = $a_info;
            $data['a_kolom'] = $a_kolom;
            $view_repp = $this->load->view('laporan/repp_view', $data, TRUE);
            $this->PDF->WriteHTML($view_repp);
            $this->PDF->Output(($tipe_laporan == "Pendapatan" ? "laporan_pendapatan" : "laporan_penjualan") . '.pdf', 'D');
        }
    }

    public function validate($code)
    {
        $x = md5(settingSIM()['qrcode_key']);
        $is_valid = $x == $code ? true : false;

        $data['title'] = 'Result';
        $data['is_valid'] = $is_valid;
        $this->load->view('laporan/validate', $data);
    }
}
