<?php

class Statistik extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('token')) {
            redirect('auth');
        }

        isLoggedIn($this->session->userdata('token'), function($result){
            if(!$result){
                return redirect('auth');
            }
        }, true);

        $this->load->model('M_users', 'users');
        $this->load->model('M_pembayaran', 'pembayaran');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url('home'));
        $this->breadcrumb->append_crumb('Statistik', site_url('statistik'));

        if ($_POST) {
            $from = $this->input->post('from');
            $to = $this->input->post('to');
            $type = $this->input->post('type');
            $repp_type = $this->input->post('repp_type');

            $arr = [
                'from' => $from,
                'to' => $to,
                'type' => $type,
                'repp_type' => $repp_type == "income" ? 'Pendapatan' : 'Penjualan'
            ];

            $data['tipe_bar'] = $type;
            $data['a_data'] = $arr;

            if ($repp_type == "income") {
                $data['statistik'] = $this->pembayaran->getLaporan($from, $to)->result_array();
                // print_r($data['statistik']);
                // die;
            } else {
                $data['statistik'] = $this->pembayaran->getStatistik($from, $to)->result_array();
            }

            $data['a_type'] = $repp_type;
        } else {
            $data['statistik'] = null;
            $data['tipe_bar'] = null;
            $data['a_data'] = null;
        }

        $data['title'] = 'Statistik';
        $data['user'] = $this->user;
        $this->template->load('template', 'statistik/index', $data);
    }
}
