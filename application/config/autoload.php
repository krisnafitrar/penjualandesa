<?php
defined('BASEPATH') or exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database', 'session', 'table', 'form_validation', 'template', 'exceptions', 'breadcrumb');

$autoload['drivers'] = array();

$autoload['helper'] = array('url', 'download', 'form', 'file', 'desaku');

$autoload['config'] = array('config_midtrans');

$autoload['language'] = array();

$autoload['model'] = array();
